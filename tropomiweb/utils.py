import psycopg2
import pymysql


class MySQLCursor():
    _conn = None
    _cursor = None
    def __enter__(self):
        self._conn = pymysql.connect(host = 'augustine.snap.uaf.edu',
                                     database = 'rsdb',
                                     user = 'scriptuser',
                                     password = '0ACE3BBD238E929DFABDFC17D5D620E6369DB88654187D280886B433CF8988C7')
        self._cursor = self._conn.cursor()
        return self._cursor
    def __exit__(self, *args, **kwargs):
        try:
            self._conn.rollback()
        except AttributeError:
            return
        self._conn.close()
        
        
class db_cursor():
    _conn = None
    _cursor = None

    def __init__(self, cursor_factory=None):
        self._cursor_factory = cursor_factory

    def __enter__(self):
        self._conn = psycopg2.connect(host='akutan.snap.uaf.edu', database='so2data',
                                      cursor_factory=self._cursor_factory, user="israel")
        self._cursor = self._conn.cursor()
        return self._cursor

    def __exit__(self, *args, **kwargs):
        try:
            self._conn.rollback()
        except AttributeError:
            return  # No connection
        self._conn.close()

