import bz2
import glob
import pickle
import time
from datetime import datetime,timedelta
from dateutil.parser import parse

from ImageGen.GenerateSectorImages import flatten_data,load_period_data
from ImageGen.config import DATA_DIR,OMPS_DIR,DEFAULT_SECTORS

import numpy
import pyresample

if __name__=="__main__":
    start=time.time()
    print("Begining data load")
    TEST_FILE="/Volumes/Transfer/img_stacks/Full Arc/tropomi_year_1km.stack"
    TEST_HEIGHT='1km'
    sector_def=next((x for x in DEFAULT_SECTORS if x['name']=='Full Arc'))
    
    with bz2.BZ2File(TEST_FILE) as sector_file:
        sector=pickle.load(sector_file)
        
    sector_data=sector['data']
    print(f"Loaded data after {time.time()-start}")
    start_date=datetime.today()-timedelta(days=365)
    stop_date=datetime.today().date()
    
    for idx,slice_start in enumerate(map(datetime.fromtimestamp,sector_data['datetime_start'])):
        if slice_start >= start_date:
            start_idx=idx
            break
        
    print(f"Found start index after {time.time()-start}")
    print(f"Starting stack from {datetime.fromtimestamp(sector_data['datetime_start'][0])} at index {start_idx}")
    
    # get rid of "old" slices
    current_data={}
    for key,value in sector_data.items():
        try:
            current_data[key]=value[start_idx:] # Should make a view, not copy the data
        except IndexError:
            print(f"Unable to slice value {key}")
            current_data[key]=value
    
    del sector_data
    
    last_data=datetime.fromtimestamp(current_data['datetime_start'][-1]) +\
        timedelta(seconds=current_data['datetime_length'][-1])
    
    print(f"Last data loaded: {last_data}")
    
    #Figure out what data is needed to bring stack up-to-date
    all_files = sorted(glob.glob(DATA_DIR))
    
    if DATA_DIR == OMPS_DIR:
        file_suffix = '.h5'
        file_type = 'OMPS'
    else:
        file_suffix = '.nc'
        file_type = "TROPOMI"
        
    # Could do this as a comprehension, but by reversing the list we arrive 
    # at our goal more quickly
    update_files=[]
    for directory in reversed(all_files):
        if parse(directory.split('/')[-1]).date()<last_data.date():
            break
        
        update_files.append(f"{directory}/*{file_suffix}")
        
    update_files.reverse()
    
    print(f"Loading new data from {len(update_files)} files after {time.time()-start}")
    load_start=last_data.timestamp()+1 #go one second later so we don't get any overlaps
    lats=(sector_def['latFrom'],sector_def['latTo'])
    longs=(sector_def['longFrom'],sector_def['longTo'])
    try:
        new_data,coda_file=load_period_data(lats,longs, update_files,
                                            TEST_HEIGHT,start=load_start)
    except TypeError:
        print("No new data to add to stack")
        exit(0)
    
    print(f"New data loaded, gridding after {time.time()-start}")
    # Grid the new data, using the same parameters as the existing
    coda_file._bin_spatial(sector['num_lat'],sector['y_from'],sector['y_increment'],
                           sector['num_lon'],sector['x_from'],sector['x_increment'],
                           {'proj':'merc', 'lon_0':'-90',
                            'preserve_units':False})
    new_data=coda_file._file_data
    
    print(f"New data gridded after {time.time()-start}")
    for key in current_data:
        current_data[key]=numpy.concatenate( (current_data[key],new_data[key]) )
        
    del new_data
    del coda_file
    
    for key in ['latitude','longitude','latitude_bounds','longitude_bounds']:
        #Keep the three-dimensional structure, but elimnate all but one slice        
        current_data[key]=current_data[key][0:1] 
        
    # save the updated stack
    print(f"Saving updated stack after {time.time()-start}")
    NEW_TEST_FILE="/Volumes/Transfer/img_stacks/Full Arc/tropomi_year_1km_new.stack"
    sector['data']=current_data
    with bz2.BZ2File(NEW_TEST_FILE,'w') as sector_file:
        pickle.dump(sector, sector_file, protocol=4)
        
    print(f"Complete after {time.time()-start}")