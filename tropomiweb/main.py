import glob
import json
import os
import re
import time
import threading
import shutil
import uuid

from datetime import datetime, timedelta
from dateutil import parser

import flask
import semver
from plotly.offline import plot
import plotly.graph_objs as go

from psycopg2 import OperationalError

from . import app, IMG_DIR
from .utils import db_cursor
from .ImageGen.GenerateSectorImages import celery_init_image_generation
from .ImageGen.config import DEFAULT_SECTORS, OMPS_DIR
from .ImageGen.config import DATA_DIR as TROPOMI_DIR


_SECTORS = ['Full Arc', 'Western Aleutians', 'Eastern Aleutians', 'AK Peninsula', 'CI to Wrangell',
            'Western AK', 'Eastern AK', 'Little Sitkin', 'Tanaga', 'Korovin', 'Cleveland',
            'Makushin', 'Pavlof', 'Veniaminof', 'Katmai', 'Cook Inlet', 'Wrangell', 'Kamchatka',
            'Hawaii', 'Marianas Islands']
_generation_procs = {}


@app.route("/")
def index():
    if not flask.request.args:
        prefix = flask.request.headers.get('X-Forwarded-Prefix')
        redirect = "/?sector=Full%20Arc"
        if prefix is not None:
            redirect = prefix + redirect
        return flask.redirect(redirect)

    selected = flask.request.args.get('sector', None)
    return flask.render_template('index.html', sectors=_SECTORS, selected=selected)


@app.route("/db_ts")
def get_db_ts():
    sector_name = flask.request.args['sector']
    level = flask.request.args['altitude']
    dtype = flask.request.args['type']

    SQL = """
    SELECT
        record_date,
        record_value
    FROM ts
    INNER JOIN sectors
    ON sectors.id=ts.sector
    WHERE sectors.name=%s
    AND ts.altitude=%s
    AND ts.type=%s
    ORDER BY record_date
    """
    args = (sector_name, level, dtype)
    x = []
    y = []
    try:
        with db_cursor() as cursor:
            cursor.execute(SQL, args)
            for ddate, dval in cursor:
                x.append(ddate)
                y.append(dval)
    except OperationalError:
        # Can't get data, return an empty div.
        return "<div></div>"

    trace = go.Scattergl(x=x, y=y, connectgaps=True)
    config = {}
    layout = dict(
        title=f'{level} SO<sub>2</sub> Mass',
        margin={
            "l": 60,
            "r": 10,
            "t": 75,
            "b": 30,
            'pad': 4
        },
        xaxis=dict(
            automargin = False,
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                         label='1m',
                         step='month',
                         stepmode='backward'),
                    dict(count=6,
                         label='6m',
                         step='month',
                         stepmode='backward'),
                    dict(step='all')
                ])
            ),
            rangeslider=dict(
                visible=False
            ),
            type='date'
        ),
        yaxis={
            "automargin": False,
            "title": {
                "text": "Kilotonnes",
            },
        }
    )

    plot_div = plot({"data": [trace],
                     "layout": layout, },
                    include_plotlyjs=False, output_type="div", auto_open=False,
                    config=config)

    return plot_div


def _get_image_from_image(ref_img, direction=None):
    imgParts = ref_img.split('/')
    # remove the first part (empty and image)
    imgParts = imgParts[1:]
    if len(imgParts) != 2:
        app.logger.error(f"Got: {imgParts} of length: {len(imgParts)}")
        raise ValueError('Image path must have 3 parts (/image/sector/imagename.png)')

    sector, img = imgParts

    # figure out which period we are looking at
    _, period, height, *_ = re.split('[-_.]', img)

    IMG_PATH = os.path.realpath(os.path.join(IMG_DIR, sector))

    file_pattern = os.path.join(IMG_PATH, f'so2_{period}_{height}_*')
    canidate_files = [x.split('/')[-1] for x in sorted(glob.iglob(file_pattern),
                                                       key=os.path.getmtime)]

    # Add the non-archived image to the list of possibilities
    # This is done seperately, rather than including it in the glob above, to ease sorting (it would sort first)
    link_file = f'so2-{period}-{height}.png'
    if img == link_file:
        cur_idx = len(canidate_files) - 1  # The link just points to the newest file, which should be sorted last.
    else:
        cur_idx = canidate_files.index(img)

    if cur_idx == -1:
        # File not found
        raise IndexError("File not found")

    last = False

    if direction is None:
        new_idx = cur_idx  # Return the current file. Primarily for use in getting the "real" filename for the link file
    elif direction == 'prev':
        new_idx = cur_idx - 1
        if new_idx <= 0:
            last = True
            new_idx = 0
    else:  # Direction=next
        new_idx = cur_idx + 1
        if new_idx >= len(canidate_files) - 1:
            last = True
            new_idx = -1

    new_file = canidate_files[new_idx]

    return (new_file, sector, last)


@app.route("/check_for_new/<version>")
@app.route("/check_for_new/<version>/<platform>")
def check_for_new_version(version, platform = "mac"):
    with db_cursor() as cursor:
        cursor.execute("SELECT value FROM metadata WHERE key='explorer_version'")
        cur_ver = cursor.fetchone()
        if cur_ver:
            cur_ver = cur_ver[0]
        else:
            return flask.jsonify({'update': False,
                                  'notes': 'Current Version Unknown', })

        cursor.execute("SELECT value FROM metadata WHERE key='explorer_notes'")
        notes = cursor.fetchone()
        notes = notes[0] if notes else ''

        key = 'explorer_link'
        if platform == 'WIN':
            key += '_win'
        SQL = f"SELECT value FROM metadata WHERE key='{key}'"

        cursor.execute(SQL)
        link = cursor.fetchone()
        link = link[0] if link else ''

    new_available = semver.compare(cur_ver, version) > 0
    ret = {'update': new_available,
           'notes': notes,
           'link': link,
           'version': cur_ver, }
    return ret


@app.route("/images/<direction>")
def navigate_images(direction):
    if direction not in ('prev', 'next'):
        return "Bad direction", 400

    curImage = flask.request.args['curImage']

    try:
        new_file, sector, last = _get_image_from_image(curImage, direction)
    except ValueError as e:
        return f"Invalid request. {e}", 400
    except IndexError as e:
        return str(e), 404

    new_url = f"images/{sector}/{new_file}"

    return flask.jsonify({
        'last': last,
        'img': new_url,
    })


@app.route("/images/file_name")
def get_image_filename():
    cur_image = flask.request.args['curImage']
    img_name, sector, _ = _get_image_from_image(cur_image)
    _, _, height, start, end, *_ = re.split('[_.]', img_name)

    if height in ['TRL', 'TRM']:
        sensor = 'OMPS'
    else:
        sensor = 'TROPOMI'
    download_file = f"{sector}_{start}_{end}_{height}_{sensor}.png"
    return download_file


@app.route("/images/<sector>/<img_name>")
def get_image(sector, img_name):
    """This should be replaced by a nginx location block when deployed."""
    img_path = os.path.realpath(os.path.join(IMG_DIR, sector))

    # Bad data, but just say not found
    if not img_path.startswith(IMG_DIR):
        return "File not found", 404

    return flask.send_from_directory(IMG_DIR, os.path.join(sector, img_name))


@app.route("/overview")
def home():
    sectors = [x for x in _SECTORS if x not in ['Eastern AK', 'Western AK', 'Kamchatka', 'Full Arc',
                                                'Western Aleutians', 'Eastern Aleutians',
                                                'AK Peninsula', 'CI to Wrangell']]

    return flask.render_template('home.html', sectors_1=sectors[:5],
                                 sectors_2=sectors[5:])


@app.route("/generateCustom", methods=["POST"])
def genCustom():
    sector = flask.request.form['sector']
    dataset = flask.request.form['dataset']
    sensor = flask.request.form['sensor']
    date_range = flask.request.form['daterange']
    date_range = int(date_range)
    excluded_dates = json.loads(flask.request.form.get('excludedDates', "[]"))

    # We just want the date portion.
    excluded_dates = [x[:10] for x in excluded_dates]

    exclude_dates = True if flask.request.form.get('excludeDates', 'false') == 'true' else False

    if sector == "Custom":
        latFrom = float(flask.request.form['latFrom'])
        latTo = float(flask.request.form['latTo'])
        lonFrom = float(flask.request.form['lonFrom'])
        lonTo = float(flask.request.form['lonTo'])
        label_level = flask.request.form['labelLevel']

        if not -90 <= latFrom <= 90 or not -90 <= latTo <= 90:
            return "Latitude value out of range", 422

        if not -360 <= lonFrom <= 360 or not -360 <= lonTo <= 360:
            return "Longitude value out of range", 422

        sector_def = {'name': sector,
                      'latFrom': latFrom,
                      'latTo': latTo,
                      'longFrom': lonFrom,
                      'longTo': lonTo, }

        if label_level in ('None', 'Some'):
            sector_def['showAllLabels'] = False
        if label_level == 'None':
            sector_def['hideAllLabels'] = True

    else:
        sector_def = next((x for x in DEFAULT_SECTORS if x['name'] == sector))

    # figure out what date range to use
    if date_range == -1:
        try:
            date_from = parser.parse(flask.request.form['dateFrom'])
            date_to = parser.parse(flask.request.form['dateTo'])
        except TypeError:
            flask.abort(422)
    else:
        date_range -= 1  # Since we are including the start/end date as one of the days
        try:
            ref_date = parser.parse(flask.request.form['refdate'])
        except TypeError:
            flask.abort(422)

        if date_range == 0:
            date_from = ref_date
            date_to = ref_date
        else:
            range_type = flask.request.form['datetype']
            if range_type == 'b':
                date_from = ref_date
                date_to = ref_date + timedelta(days=date_range)
            elif range_type == 'e':
                date_to = ref_date
                date_from = ref_date - timedelta(days=date_range)
            else:
                flask.abort(400)

    # Check for good date range
    data_dir = OMPS_DIR if sensor == 'OMPS' else TROPOMI_DIR
    avail_dates = [parser.parse(x.split('/')[-1]) for x in glob.glob(data_dir)]
    first_date = min(avail_dates)
    last_date = max(avail_dates)

    if date_to < first_date or date_from > last_date:
        return "No data available for requested date range", 404

    warnings = []
    if date_from < first_date or date_to > last_date:
        warnings.append("A portion of selected dates fall outside available data.\n\nImage will be limited to dates available.")

    date_range = (date_from, date_to)
    if exclude_dates:
        date_range += tuple(excluded_dates)

    file_name = f"so2_{sector}_{dataset}_{date_from.date().isoformat()}_{date_to.date().isoformat()}_{datetime.now().timestamp()}.png"

    task = celery_init_image_generation.delay([sector_def], date_range, file_name, dataset, data_dir)
    task.complete = False
    taskid = uuid.uuid4().hex
    task.taskid = taskid

    monitor = threading.Thread(target=_monitor_image_gen,
                               args=(task, file_name),
                               daemon=True)
    monitor.start()

    _generation_procs[file_name] = task

    return flask.jsonify({'file': file_name, 'warnings': warnings, 'taskid': taskid}), 200


def _monitor_image_gen(task, filename):
    # task.get()  # blocks until complete. Also blocks the started signal from the process. Oh well.
    while not task.ready():
        time.sleep(.25)

    if not task.failed():
        final_dest = os.path.realpath(os.path.join(IMG_DIR, 'custom'))
        os.makedirs(final_dest, exist_ok=True)
        try:
            shutil.move(f'/tmp/{filename}', os.path.join(final_dest, filename))
        except FileNotFoundError:
            pass
        del _generation_procs[filename]

    task.complete = True


@app.route('/check_complete')
def check_process():
    file_name = flask.request.args['file']
    process = _generation_procs.get(file_name)
    if process is None:
        # process is removed from dictionary when complete to avoid the dictionary growing potentially
        # without bounds if someone requests an image but doesn't check on it. Therfore, if not found
        # assume complete.
        return flask.jsonify({'complete': True, 'failed': False, })

    # isinstance(process, multiprocessing.pool.AsyncResult)
    ready = process.complete
    started = process.state == 'STARTED'
    failed = process.failed()
    if failed:  # Ok, the client knows now, we can remove this reference
        del _generation_procs[file_name]

    return flask.jsonify({'complete': ready, 'failed': failed, 'started': started, })


@app.route('/customImage/<image>')
def get_custom_image(image):
    # Make sure all requests are complete
    if image in _generation_procs:
        task = _generation_procs[image]
        del _generation_procs[image]
        if not task.ready():
            return "Image not ready", 202
        if task.failed():
            return "Image generation failed", 410

    IMG_PATH = os.path.realpath(os.path.join(IMG_DIR, 'custom'))

    # Bad data, but just say not found
    if not IMG_PATH.startswith(IMG_DIR):
        return "File not found", 404

    return flask.send_from_directory(IMG_PATH, image)
