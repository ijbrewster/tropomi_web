from datetime import datetime, timedelta
from functools import lru_cache
from threading import Lock

import flask
import numpy
import pandas

import matplotlib.pyplot as plt
import matplotlib.cm as cmx
import matplotlib.colors as colors

from cachetools import cached
from cachetools.func import ttl_cache

from . import utils
from . import app
from .DateCache import DateCache


@lru_cache(1)  # Only need to do this once, so might as well cache it.
def gen_colormap():
    colormapname = 'hot_r'
    MAXMAGCOLORBAR = 1000
    MINMAGCOLORBAR = 150
    mycolormap = cm = plt.get_cmap(colormapname)
    cNorm = colors.Normalize(vmin=MINMAGCOLORBAR, vmax=MAXMAGCOLORBAR)
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=mycolormap)
    return scalarMap


@lru_cache()
def get_luminance_of_color(color):
    new_color = []
    for c in color:
        c = c / 255.0
        if c <= 0.04045:
            c = c / 12.92
        else:
            c = pow(((c + 0.055) / 1.055), 2.4)
        new_color.append(c)

    r, g, b = new_color
    L = 0.2126 * r + 0.7152 * g + 0.0722 * b
    return L


def get_rgb_from_cm(cm, val):
    return numpy.asarray(cm.to_rgba(val))[:, :-1] * 255


@app.route('/weeklysummary')
def weekly_summary():
    return flask.render_template('weeklysum.html')


@app.route('/weeklysummary/data')
def get_summary_data():
    weeks_to_plot = int(flask.request.args.get('weeks', 13))
    page_num = int(flask.request.args.get('page', 0))
    default_end = datetime.utcnow().strftime('%Y-%m-%d')
    end_date = flask.request.args.get('endDate') or default_end

    end_date = datetime.strptime(end_date, '%Y-%m-%d')
    page_time = timedelta(weeks = weeks_to_plot)

    if page_num:
        end_date -= (page_time * page_num)

    start_date = end_date - page_time

    week_break = f"W-{end_date.strftime('%a').upper()}"
    percentile_labels = (50, 60, 70, 80, 90, 100)
    percentiles = get_percentile_levels(week_break, percentile_labels)
    detections = get_detections()

    # Only keep the data in the date range we care about
    detections = detections[detections['date'] >= start_date]
    detections = detections[detections['date'] <= end_date]

    full_index = pandas.date_range(start_date, end_date, freq = week_break)
    colormap = gen_colormap()

    # Generate week labels
    week_labels = full_index.strftime('%m/%d').tolist()
    week_labels.reverse()
    time_grouper = pandas.Grouper(key = 'date', freq = week_break)
    weekly_volc_data = {}
    for volc_id, group in detections.groupby('volcano_id'):
        print(group['rate'].max())
        # Find the display name for this volcano.
        # next of generator because sometimes the volcano_name column has a NULL/empty value
        volc_name = next((x for x in group['volcano_name'] if x))

        # Fill in any missing date values
        week_group = group.groupby(time_grouper)
        volc_data = pandas.DataFrame()
        volc_count = week_group.count()['dt']
        volc_data['count'] = volc_count.reindex(full_index, fill_value = 0)
        week_sum = week_group.sum(numeric_only = True)

        cumulative_mass = week_sum['mass_prelim'].reindex(full_index, fill_value = 0)
        median_mass = week_group.median(numeric_only = True)['mass_prelim'].reindex(full_index,
                                                                                    fill_value = 0)
        median_mass[numpy.isnan(median_mass)] = 0
        volc_data['mass'] = median_mass

        em_rate = week_group.mean(numeric_only = True)['rate']
        em_rate = em_rate.reindex(full_index, fill_value = numpy.NaN)
        missing_rate = numpy.isnan(em_rate)
        em_rate[missing_rate] = 0

        volc_data['em_rate'] = em_rate
        box_color = get_rgb_from_cm(colormap, volc_data['em_rate'])
        box_color[missing_rate] = (128, 128, 128)
        volc_data['color'] = box_color.tolist()

        color_luminance = [
            get_luminance_of_color(tuple(x))
            for x in volc_data['color']
        ]

        text_color = numpy.full_like(volc_data['mass'], 'black', dtype = object)
        text_color[numpy.asarray(color_luminance) <= .179] = 'white'
        volc_data['textColor'] = text_color

        try:
            volc_data['percentile'] = pandas.cut(median_mass,
                                                 bins = percentiles[volc_id],
                                                 labels = percentile_labels,
                                                 duplicates = 'drop').astype(float)
        except:
            volc_data['percentile'] = numpy.full_like(volc_data.index, 0, dtype = float)
        volc_data['percentile'] = volc_data['percentile'].replace(numpy.nan, 0).astype(int)
        volc_data.sort_index(inplace = True, ascending = False)

        weekly_volc_data[volc_name] = volc_data.to_dict(orient='records')

    ret_obj = {'weeks': week_labels,
               'results': weekly_volc_data,
               'from': start_date.strftime('%m/%d/%Y'),
               'to': end_date.strftime('%m/%d/%Y'), }
    return ret_obj


@ttl_cache()
def get_detections() -> pandas.DataFrame:
    SQL = """
SELECT DISTINCT
    date as dt,
    substring_index(substring_index(rv.text,'[b]',-1),
                    ' (',1) as volcano_name,
    volcano_id,
    mass_prelim,
    plume_age,
    ((mass_prelim*1000)/plume_age)*24 as rate
FROM report
LEFT JOIN report_volcano AS rv ON report.report_id=rv.report_id
LEFT JOIN report_volcano_keyword AS rvk ON rv.report_volcano_id=rvk.report_volcano_id
LEFT JOIN report_volcano_keyword_meta AS rvkm  ON rvk.report_volcano_keyword_id=rvkm.report_volcano_keyword_id
WHERE report.published='yes'
AND keyword_id=9
ORDER BY volcano_id, date DESC;
    """

    with utils.MySQLCursor() as cursor:
        cursor.execute(SQL)
        headers = [x[0] for x in cursor.description]
        detections = pandas.DataFrame(cursor, columns = headers)

    detections['date'] = detections['dt'].dt.normalize()
    detections['mass_prelim'] = pandas.to_numeric(detections['mass_prelim'])

    return detections


def _percentile_key(a, b):
    return (a, tuple(b))


@cached(cache = DateCache(2), key = _percentile_key, lock = Lock())
def get_percentile_levels(week_break, levels = [25, 50, 75, 100]):
    app.logger.info("Updating percentile levels")
    detections = get_detections()

    percentiles = {}
    time_grouper = pandas.Grouper(key = 'date', freq = week_break)
    volc_group = detections.groupby('volcano_id')
    for name, group in volc_group:
        median_mass = group.groupby(time_grouper).median(numeric_only = True).reset_index()['mass_prelim']
        # Ignore weeks with zero mass when calculating percentiles
        median_mass = median_mass[median_mass > 0]
        percentile_values = [0] + [median_mass.quantile(x / 100)
                                   for x in levels]
        # Fix any duplicates
        for i in range(1, len(percentile_values)):
            if percentile_values[i] <= percentile_values[i - 1]:
                percentile_values[i] = percentile_values[i - 1] + .001  # just make it a hair bigger
        percentiles[name] = percentile_values

    return percentiles
