import flask

app = flask.Flask(__name__)
IMG_DIR = '/data/tropomi_data/webstack_images/'

from . import main
from . import weeklySummary
from . import volcview_corners
