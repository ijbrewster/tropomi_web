var inital = true
var excludeDatePicker
var CUSTOM_LEVELS = {
    'TROPOMI': ["1km", "7km", "15km"],
    'OMPS': ['PBL', 'TRL', 'TRM', 'TRU', 'STL']
};

$(document).ready(function() {
    $(document).on('click', '#sectorList li.sector', selectSector);
    $(document).on('click', '#sectorList li.customSector', customSector);
    $(document).on('click', 'div.download', downloadImage);
    $(document).on('click', 'div.navButton:not(.disabled)', changeImage);
    $('.latitude').change(verifyLatitude);
    $('.longitude').change(verifyLongitude);

    window.onpopstate = function(e) {
        var sector = window.location.search.split('=')[1]
        console.log(sector)
        $('#sectorList li').each(function(idx, item) {
            if (encodeURI($(item).text()) == sector) {
                $(this).click()
                return false;
            }
        })
    };

    $('#customAreaSelect').change(function() {
        if (this.value == "Custom") {
            $('#customArea').show();
        } else {
            $('#customArea').hide();
        }
    })

    $('#generateCustomImage').click(submitCustom)

    $('.date').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd",
        maxDate: 0,
        minDate: -730
    })

    $('#so2year7km').on('load', function() {
        $('#noSector').hide();
    });

    $('#homeLink').click(function() { location = '/' })

    $(document).on('plotly_relayout', '.plotly-graph-div', scale_y);
    $('#dateRange').change(changeDateRange);
    changeDateRange.call($('#dateRange')[0]);
    excludeDatePicker = new Datepickk({
        container: document.querySelector('#excludeDates'),
        inline: true,
        today: true,
        'maxDate': new Date()
    })

    $('#exclude').click(function() {
        //could just use slideToggle() here, but I'm always affraid someone will do something odd
        //and it will get out-of-sync.
        if ($(this).is(':checked')) {
            excludeDatePicker.unselectAll();
            $('#excludeDates').slideDown();
        } else {
            $('#excludeDates').slideUp();
            excludeDatePicker.unselectAll();
        }
    });

    $('button.tabButton.dataset').click(setDataset);

});

var LEVELS = {
    'OMPS': ['TRL', 'TRM'],
    'TROPOMI': ['1km', '7km']
}

function setDataset() {
    var dataset = $(this).data('dataset');
    var levels = LEVELS[dataset];
    var customLevels = CUSTOM_LEVELS[dataset];
    $('#level1tag').text(levels[0])
    $('#level2tag').text(levels[1])
    $('#tabs').data('current', dataset);
    $('#tabs .tabButton').removeClass('current');
    $(this).addClass('current');

    //remake the custom altitude selector list
    $('#datasetSelect').empty();
    for (var level of customLevels) {
        var option = $('<option>').text(level);
        $('#datasetSelect').append(option);
    }

    $('#sectorList li.sector.selected').click();
}

function verifyLatitude() {
    var lat = Number(this.value)

    if (isNaN(lat) || lat > 90 || lat < -90) {
        alert("Please enter a valid latitude.\n\nValid latitudes range from -90 to +90");
    }
}

function verifyLongitude() {
    var lon = Number(this.value)

    if (isNaN(lon) || lon < -360 || lon > 360) {
        alert("Please enter a valid longitude.\n\nValid longitudes range from -360 to +360");
    }
}

function changeDateRange() {
    var range = this.value;


    if (range == "-1") {
        $('#customDateRange').show();
        $('#dateType').hide();
        $('#referenceDate').hide();
    } else {
        $('#customDateRange').hide();
        $('#dateType').show();
        $('#referenceDate').show();
    }

    if (range === '1') {
        $('#dateType').append('<option value="-1">On</option>').val(-1).attr('disabled', true)
    } else {
        $('#dateType').attr('disabled', false)
        $('#dateType option[value="-1"]').remove()
    }
}

function changeImage() {
    var direction;
    var button_class;
    var navButton = $(this)
    if (navButton.hasClass('nextImage')) {
        direction = 'next';
        button_class = 'nextImage';
        //if we go forward, then we should be able to go back
        $('.navButton.prevImage').removeClass('disabled')
    } else if (navButton.hasClass('prevImage')) {
        direction = 'prev'
        button_class = 'prevImage';
        $('.navButton.nextImage').removeClass('disabled')
    } else { return; } //this isn't a nav button


    $('div.navButton.' + button_class + ':not(.disabled)').each(function() {
        var navButton = $(this);
        var imgDiv = navButton.closest('div.so2img').find('img')
        var curImage = imgDiv.attr('src')

        $.get('images/' + direction, { 'curImage': curImage }, function(imgData) {
            imgDiv.prop('src', imgData['img'])
            if (imgData['last']) {
                navButton.addClass('disabled')
            }
        });
    });
}

function downloadImage() {
    var image = $(this).closest('.imgDiv').find('img')
    var img_src = image.attr('src');
    $.get('images/file_name', { 'curImage': img_src })
        .done(function(filename) {
            var a = document.createElement('a');
            a.href = img_src;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        })
}

function scale_y(event, detail) {
    var start, stop;
    if (detail && detail.hasOwnProperty('xaxis.range')) {
        start = detail['xaxis.range'][0];
        stop = detail['xaxis.range'][1];
    } else if (detail && detail.hasOwnProperty('xaxis.range[0]')) {
        start = detail['xaxis.range[0]'];
        stop = detail['xaxis.range[1]'];
    } else {
        return;
    }

    //only one trace on these
    var x_data = this.data[0]['x'];
    var y_data = this.data[0]['y'];

    var start_idx = null;
    var stop_idx = null;

    for (var i = 0 in x_data) {
        var value = x_data[i];
        if (start_idx === null && value >= start) {
            start_idx = i;
        }

        if (stop_idx === null && value >= stop) {
            stop_idx = i;
            break;
        }
    }

    if (start_idx === null) { //no values greater than or equal to our start value. Should be impossible.
        start_idx = x_data.length - 1;
    }

    if (stop_idx === null) {
        stop_idx = x_data.length - 1;
    }

    var y_values = y_data.slice(Number(start_idx), Number(stop_idx) + 1);
    var max_y = Math.max(...y_values);
    var min_y = -2;
    var y_top = max_y;
    if (max_y > 20) {
        y_top = Math.ceil(max_y / 5) * 5; //round to the closest 10
        if ((y_top - max_y) <= 1) {
            y_top += 2;
        } else {
            y_top += .75;

        }
        min_y = -2.5;
    } else {
        y_top = Math.ceil(max_y); //round to the closest 1
        min_y = -1;
    }

    Plotly.relayout(this, 'yaxis.range', [min_y, y_top]);
}

function hilightSector(sector) {
    $('li').removeClass('selected')
    $(sector).addClass('selected')

    if ($(sector).hasClass('customSector')) {
        $('#customSector').show('slide', { direction: 'up' }, 400, function() {
            $('#imageHeader').text("Custom Stacked Image")
        });

        $('#images').slideUp(400)
    } else {
        $('#customSector').hide('slide', { direction: 'down' }, 400, function() {
            $('#imageHeader').text("Stacked Sector Images")
        });
        $('#images').slideDown()
    }
}

function customSector() {
    hilightSector(this);
}

function selectSector() {
    hilightSector(this);

    //reset navigation buttons
    $('.nextImage').addClass('disabled'); //we load the most recent image by default, so no next
    $('.prevImage').removeClass('disabled');

    var sector = $(this).text()
    var URL_BASE = 'images/' + sector + "/"

    //update the URL
    var origin = window.location.origin
    var path = window.location.pathname
    var search = encodeURI("?sector=" + sector)
    if (search !== window.location.search) {
        initial = false;
        window.history.pushState({}, "", origin + path + search);
    }

    var periods = ['day', 'week', 'month', 'year']
    var levels = $('#tabs').data('current') == 'OMPS' ? ['TRL', 'TRM'] : ['1km', '7km'];
    var types = ['1km', '7km'];
    var data_type = $('#tabs').data('current')
    if (typeof(data_type) == 'undefined')
        data_type = 'TROPOMI'

    for (var j in types) {
        var type = types[j]
        var level = levels[j]

        //load the time series for this type
        ts_url = URL_BASE + "timeseries-" + level + ".html"
        var plotlyDiv = $('#timeSeries' + type + 'Div div .plotly-graph-div')[0]
            //if we have an existing graph, purge it before loading the new one
        if (typeof(plotlyDiv) !== 'undefined')
            Plotly.purge(plotlyDiv)

        //$('#timeSeries'+type+'Div').load(encodeURI(ts_url));
        //capture the proper desitnation div
        function load_ts() {
            var div = '#timeSeries' + type + 'Div';
            $(div).empty();
            $.get('db_ts', {
                    'sector': sector,
                    'altitude': level,
                    'type': data_type
                })
                .done(function(data) {
                    $(div).html(data);
                });
        };

        load_ts();

        for (var i in periods) {
            var period = periods[i]
            img_url = URL_BASE + "so2-" + period + "-" + level + ".png"
            $('#so2' + period + type).data('sector', sector).attr('src', img_url)
        }
    }
}

function submitCustom() {
    var customData = {
        'sector': $('#customAreaSelect').val(),
        'latFrom': $('#latitudeFrom').val(),
        'latTo': $('#latitudeTo').val(),
        'lonFrom': $('#longitudeFrom').val(),
        'lonTo': $('#longitudeTo').val(),
        'dateFrom': $('#dateFrom').val(),
        'dateTo': $('#dateTo').val(),
        'dataset': $('#datasetSelect').val(),
        'daterange': $('#dateRange').val(),
        'datetype': $('#dateType').val(),
        'refdate': $('#referenceDate').val(),
        'labelLevel': $('#labelSelect').val(),
        'excludedDates': JSON.stringify(excludeDatePicker.selectedDates),
        'excludeDates': $('#exclude').is(':checked'),
        'sensor': $('.dataset.current').data('dataset')
    }

    var good = true;
    var errors = []

    if (customData['daterange'] == "-1") {
        if (customData['dateFrom'] == '') {
            errors.push("You must specify a start date!");
            good = false;
        }

        if (customData['dateTo'] == '') {
            errors.push("You must specify an end date!");
            good = false;
        }
    } else {
        if (customData['refdate'] == '') {
            errors.push("You must specify your starting/ending date!");
            good = false;
        }
    }

    if (customData['sector'] == 'Custom') {
        //make sure all required data is filled out
        if (customData['latFrom'] === '' || customData['latTo'] === '' ||
            customData['lonFrom'] === '' || customData['lonTo'] === '') {
            var keys = ['latFrom', 'latTo', 'lonFrom', 'lonTo']
            var values = ['Latitude From', 'Latitude To', 'Longitude From', 'Longitude To']
            for (var i in keys) {
                if (customData[keys[i]] == '') {
                    errors.push("When selecting a custom sector, you must specify " + values[i]);
                }
            }
            good = false;
        }

        //check for proper latitude ordering
        if (Number(customData['latFrom']) >= Number(customData['latTo'])) {
            errors.push("Please enter latitudes in south to north order (i.e. from 40 to 50)")
            good = false;
        }

        //check for proper longitude ordering
        if (Number(customData['lonFrom']) >= Number(customData['lonTo'])) {
            errors.push("Please enter longitudes in east to west order (i.e. from -180 to -140)")
            good = false;
        }
    }

    if (!good) {
        var errorString = '•' + errors.join('\n•')
        alert("Errors have been detected in the form. Please fix these to continue:\n\n" + errorString);
        return;
    }


    $('#generateCustomImage').attr('disabled', true);
    $('#customResult').empty().append("<div class='loader'>");

    $.post('/generateCustom', customData)
        .done(function(data) {
            setTimeout(function() { check_for_result(data['file'], 1) }, 3000);
            if (data.warnings.length > 0) {
                $('#customResult').append("WARNING: " + data.warnings.join("<br>"))
            }
        })
        .fail(function(result) {
            alert("Custom image generation failed.\n" + result.status + " - " + result.statusText + "\n\n" + result.responseText);
            $('#customResult').empty();
        })
        .always(function() {
            $('#generateCustomImage').attr('disabled', false);
        })
}

function check_for_result(file, itteration) {
    $.get('/check_complete', { 'file': file })
        .done(function(result) {
            var url = window.location.href.split("/")
            var linksrc = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') +
                '/customImage/' + file
            var permLink = $('<a>')
            permLink.text(linksrc);
            permLink.attr('href', linksrc);
            permLink.attr('download', file);
            permLink.attr('target', '_blank');

            if (result.complete) {
                if (result.error) {
                    alert("Unable to generate image, generation process returned an error.\nCheck the server logs for more details")
                    return;
                }
                $('#generateCustomImage').attr('disabled', false);
                var image = new Image()
                image.src = '/customImage/' + file
                $('#customResult').empty()
                    .append("You can download this image for the next 7 days from: ")
                    .append(permLink)
                    .append('<br>')
                    .append(image);
            } else {
                //result not ready yet. Try again later
                itteration++;
                if (itteration == 2) {
                    $('#customResult').prepend(permLink)
                        .prepend("Once complete, you can download this image from: ")
                        .prepend("Please Wait. Depending on size of sector and time frame selected, this can take up to several minutes<br>");
                }

                if (itteration == 5 && !result.started) {
                    alert("Image generation failed to start. Please check the server logs for more information");
                    return;
                }
                setTimeout(function() { check_for_result(file, itteration) }, 3000)
            }
        })
}
