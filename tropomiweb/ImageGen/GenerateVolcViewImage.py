import gc
import math
import multiprocessing as mp
import os
import time
import sys
import traceback
import warnings
from datetime import datetime
from io import BytesIO

import numpy
import pyproj

try:
    from PySide6.QtGui import (QPainterPath,
                               QFont,
                               QPen,
                               QTransform)

    from PySide6.QtSvg import QGraphicsSvgItem

    from PySide6.QtWidgets import (QApplication,
                                   QWidget,
                                   QVBoxLayout)

    from PySide6.QtCore import (Qt, QSize, QByteArray, QBuffer, QIODevice)
except ImportError:
    from PySide2.QtGui import (QPainterPath,
                               QFont,
                               QPen,
                               QTransform)

    from PySide2.QtSvg import QGraphicsSvgItem

    from PySide2.QtWidgets import (QApplication,
                                   QWidget,
                                   QVBoxLayout)

    from PySide2.QtCore import (Qt, QSize, QByteArray, QBuffer, QIODevice)

import pyqtgraph as pg

from .GradientScale import GradientWidget
from .config import (VOLCANOS, MAP_IMG)
from .LatLonAxisItem import LatLonAxisItem
from .h5pyimport import NetCDFFile


def flatten_data(data):
    # Average any data that meets the shape requirements
    for key in ['latitude', 'longitude']:
        data[key] = data[key][0].reshape(data[key][0].size)
    for key in ['latitude_bounds', 'longitude_bounds']:
        data[key] = data[key][0].reshape((-1, 4))

    target_shape = data['SO2_column_number_density'].shape
    for item in data:
        if data[item].shape == target_shape:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                flat_item = numpy.nanmean(data[item], 0)
            data[item] = flat_item.reshape(flat_item.size)

    # Couple of oddballs
    for oddity in ('SO2_volume_mixing_ratio_dry_air_apriori', 'pressure'):
        try:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)
                flat_item = numpy.nanmean(data[oddity], 0)
        except KeyError:
            continue

        data[oddity] = flat_item.reshape(int(flat_item.size / flat_item.shape[-1]),
                                         flat_item.shape[-1])

    # Orbit index no longer makes sense
    # Nor does datetime start (since the "measurement" is now *several* measurements)
    del data['datetime_start']
    # Same for datetime_length
    del data['datetime_length']

    return data


def _generate_path(coord):
    path = QPainterPath()
    path.moveTo(*coord[0])
    path.lineTo(*coord[1])
    path.lineTo(*coord[2])
    path.lineTo(*coord[3])
    path.closeSubpath()
    return path


LAT_LON_PROJ = pyproj.Proj(init='epsg:4326', preserve_units=False)
MERCATOR_PROJ = pyproj.Proj(init='epsg:3857 +lon_0=-90', preserve_units=False)
MERC_TRANSFORMER = pyproj.Transformer.from_proj(LAT_LON_PROJ, MERCATOR_PROJ)


def load_sector_data(lats, lons, file, height, validity=1, cloud_fraction=None, density=None):
    lat_from, lat_to = lats
    lon_from, lon_to = lons

    filters = [f"latitude>={lat_from-2}",
               f"latitude<={lat_to+2}",
               f"longitude_range({lon_from-2},{lon_to+2})",
               "sensor_zenith_angle<62",
               "solar_zenith_angle<70",
               ]

    if density is not None:
        filters.append(f'SO2_column_number_density>={density}')

    if validity is not None:
        filters.append(f"SO2_column_number_density_validity>={validity}")
    else:
        filters.append("valid(SO2_column_number_density)",)

    if cloud_fraction is not None:
        filters.append(f"cloud_fraction<{cloud_fraction}")

    filter_string = ";".join(filters)
    options = f'so2_column={height}'
    try:
        so2data_file = NetCDFFile(file)
        data = so2data_file.import_data(filter_string, options)
    except Exception:
        print(f"*****Got error when importing {height} product******")
        print(traceback.print_exc())
        return None
    return (data, so2data_file)


def generate_sector_image(lats, lons, data, out_dir, show_volc_names, hide_all_names, filename,
                          sector, file_date, height, gen_others=False):
    lat_from, lat_to = lats
    lon_from, lon_to = lons

    pixel_bounds = numpy.stack((data['latitude_bounds'],
                                data['longitude_bounds']),
                               axis=-1)

    x_lat_lon = pixel_bounds[:, :, 1].reshape(pixel_bounds[:, :, 1].size)
    y_lat_lon = pixel_bounds[:, :, 0].reshape(pixel_bounds[:, :, 0].size)

    x_merc, y_merc = MERC_TRANSFORMER.transform(x_lat_lon, y_lat_lon)

    x_merc = x_merc.reshape(int(x_merc.size / 4), 4)
    y_merc = y_merc.reshape(int(y_merc.size / 4), 4)
    merc_pixel_bounds = numpy.stack([x_merc, y_merc], axis=2)

    # Generate path objects for each pixel for graphing purposes.
    # To get the shape of each pixel, shift each one to 0,0 lower left bounding box
    shifted_coords = merc_pixel_bounds - numpy.min(merc_pixel_bounds, axis=1)[:, None, :]
    # We have to do min twice to get the single min value for each group of corner points
    # If we only did it once, X and Y would be scaled seperately, distorting the shape.
    scale_factors = numpy.max(numpy.max(shifted_coords, axis=1), axis=1)
    # Scale each pixel to fit within -0.5 - +0.5
    scaled_coords = (shifted_coords * (1 / scale_factors[:, None, None])) - .5
    # "Center" the scaled coordinates so the paths correctly represent the points
    scaled_coords -= (((numpy.max(scaled_coords, axis=1)
                        - numpy.min(scaled_coords, axis=1)) - 1) / 2)[:, None, :]

    pixel_paths = [_generate_path(x) for x in scaled_coords]

    calc_x, calc_y = MERC_TRANSFORMER.transform(data['longitude'], data['latitude'])

    x_from, y_from = MERC_TRANSFORMER.transform(lon_from, lat_from)
    x_to, y_to = MERC_TRANSFORMER.transform(lon_to, lat_to)
    x_range = [x_from, x_to]
    y_range = [y_from, y_to]

    desired_ratio = 1.25  # width to height

    # <1 => height greater than width.
    # >1 => height less than width.
    width_height_ratio = abs(x_range[1] - x_range[0]) / abs(y_range[1] - y_range[0])

    if width_height_ratio < desired_ratio:
        # too tall for width. Add width to compensate.
        desired_width = abs(y_range[1] - y_range[0]) * desired_ratio
        actual_width = abs(x_range[1] - x_range[0])
        width_change = desired_width - actual_width
        # apply one-half of the difference to each end
        x_range[0] += width_change / 2
        x_range[1] -= width_change / 2
    elif width_height_ratio > desired_ratio:
        # too wide for height. Add height to compensate.
        desired_height = abs(x_range[1] - x_range[0]) / desired_ratio
        actual_height = abs(y_range[1] - y_range[0])
        height_change = desired_height - actual_height
        # apply one-half of the difference to each end
        x_range[0] -= height_change / 2
        x_range[1] += height_change / 2

    img_width = 1000
    img_height = 800
    # img_height = img_width / width_height_ratio
    # if img_height > MAX_HEIGHT:
    # img_height = MAX_HEIGHT
    # img_width = img_height * width_height_ratio

    view_size = QSize(img_width, img_height)

    # Create the image widgets
    plot_item, scale_widget, disp_widget, volc_labels = _initalize_image_widgets()

    for idx, volcano in enumerate(VOLCANOS):
        if (show_volc_names or (volcano.get('showAlways', False)) and not hide_all_names):
            label = volc_labels[idx]
            plot_item.addItem(label)

    view_widget = plot_item.getViewWidget()
    view_widget.parent().setFixedSize(view_size)
    view_widget.adjustSize()

    vbox = plot_item.getViewBox()
    vbox.setAspectLocked(True, 1)
    vbox.disableAutoRange()

    vbox.setRange(xRange=x_range, yRange=y_range, padding=0)

    ###############################################################
    # Plot Generation Code
    ###############################################################
    def plot_dataset(dataset, color_map, scale_labels):
        # Only generate the brush once for each unique value
        lookup_table = {x: pg.mkBrush(color_map.map(x)) for x in numpy.unique(dataset)}
        brushes = [lookup_table[x] for x in dataset]

        scale_widget.setGradient(color_map.getGradient())
        scale_widget.setLabels(scale_labels)

        # Generate Plot
        plot = plot_item.plot(calc_x, calc_y,
                              pen=None,
                              symbolPen=None,
                              symbolBrush=brushes,
                              pxMode=False,
                              symbolSize=scale_factors,
                              symbol=pixel_paths)
        plot.setZValue(-1e7)

        plot_item.getViewWidget().parent().grab()
        volcview_img = plot_item.getViewWidget().parent().grab()
        _volcview_upload(volcview_img, sector, band, data_type, file_date)
        volcview_img.save(dest_file)
        plot_item.removeItem(plot)
    ###############################################################
    if height in ['1km', '7km']:
        band = 'TROPOMI'
    else:
        band = 'OMPS'

    if not gen_others:
        # generate images for so2 density
        # Generate SO2 brushes
        du_val = data['SO2_column_number_density'] * 2241.15  # Conversion Factor from manual
        normalized_du = du_val * (1 / 20)
        normalized_du[normalized_du > 1] = 1
        normalized_du[normalized_du < 0] = 0

        # When rounded to 5 digits, the color results are identical.
        normalized_du = numpy.round(normalized_du, 5)

        du_color_map = pg.ColorMap([0, .05, .1, .175, .25, .99, 1],
                                   [(255, 255, 255),
                                    (241, 187, 252),
                                    (53, 248, 244),
                                    (255, 225, 0),
                                    (248, 152, 6),
                                    (255, 19, 0),
                                    (255, 0, 0)])

        scale_labels = {0: "0 DU", .05: "1", .1: "2", .25: "5", .6: "12", 1: ">20 DU", }
        dest_file = os.path.join(out_dir, f"{band}-{height}.png")

        data_type = height
        plot_dataset(normalized_du, du_color_map, scale_labels)

    else:
        cloud = data['cloud_fraction']  # Already 0 to 1
        cloud_color_map = pg.ColorMap([0, 1], [(255, 255, 255), (0, 0, 0)])
        scale_labels = {0: "0%", 1: "100%", }
        dest_file = os.path.join(out_dir, f"{band}-cloud.png")

        data_type = 'cloud'

        plot_dataset(cloud, cloud_color_map, scale_labels)

        validity = data['SO2_column_number_density_validity'] / 100
        validity_color_map = pg.ColorMap([0, 1], [(255, 255, 255), (255, 0, 0)])
        # Can use the same labels as for cloud
        dest_file = os.path.join(out_dir, f"{band}-validity.png")
        data_type = 'validity'

        plot_dataset(validity, validity_color_map, scale_labels)


def _initalize_image_widgets():
    """Set up the various QT widgets used to display the plot"""
    # Set up display widgets
    try:
        QApplication(sys.argv + ['-platform', 'offscreen'])  # So we can make widgets :)
    except RuntimeError as err:
        if "singleton" not in str(err):
            raise

    font = QFont("Arial", 10)
    scale_font = QFont("Arial", 11)

    disp_widget = QWidget()

    v_layout = QVBoxLayout()
    v_layout.setStretch(1, 0)
    v_layout.setContentsMargins(0, 0, 0, 5)
    v_layout.setSpacing(0)

    view_box = pg.ViewBox(border={'color': "000", 'width': 1},
                          lockAspect=1)
    # Set up the custom axis items that translate x,y mercator coordinates to lat lon.
    lon_spacing = [(1113194.907932737, 0),
                   (222638.98158654757, 0),
                   (111319.49079327285, 0)
                   ]

    lat_spacing = [(10, 0),
                   (2, 0),
                   (1, 0)
                   ]

    grid_pen = QPen(Qt.SolidLine)

    right_axis = LatLonAxisItem("right", pen=grid_pen)
    right_axis.setTickSpacing(levels=lat_spacing)
    right_axis.setGrid(38)
    right_axis.setTickFont(font)

    top_axis = LatLonAxisItem("top", pen=grid_pen)
    top_axis.setGrid(38)
    top_axis.setTickSpacing(levels=lon_spacing)
    top_axis.setTickFont(font)

    left_axis = LatLonAxisItem("left")
    left_axis.setTickSpacing(levels=lat_spacing)
    left_axis.setTickFont(font)

    bottom_axis = LatLonAxisItem("bottom")
    bottom_axis.setTickSpacing(levels=lon_spacing)
    bottom_axis.setTickFont(font)

    plot_widget = pg.PlotWidget(disp_widget,
                                axisItems={"right": right_axis,
                                           'top': top_axis,
                                           'left': left_axis,
                                           'bottom': bottom_axis,
                                           },
                                viewBox=view_box)

    top_axis.setZValue(95)
    right_axis.setZValue(95)

    scale_widget = GradientWidget(disp_widget)
    scale_widget.setOrientation("Horizontal")
    scale_widget.setFont(scale_font)
    scale_widget.setStyleSheet("background-color:white;")

    v_layout.addWidget(plot_widget)
    v_layout.addWidget(scale_widget)

    disp_widget.setLayout(v_layout)

    plot_item = plot_widget.getPlotItem()

    plot_item.showAxis('right')
    plot_item.showAxis('top')
    plot_item.hideButtons()

    svg_item = QGraphicsSvgItem(MAP_IMG)

    # These values are the limits for the mercator projection, +/-180 degree longitude.
    # No magic here:-)
    x_start = -20037508.34278924
    x_stop = 20037508.342789244
    x_size = x_stop - x_start

    # 85 degrees north
    y_stop = 19971868.880408563

    transform = QTransform()

    # This, however, IS a magic number, just to get things to line up properly north-south.
    # Probably a mis-configuration of the viewBox parameter for the SVG.
    transform.translate(x_start, y_stop - 2000)

    svg_width = svg_item.boundingRect().width()
    SCALE_FACTOR = x_size / svg_width

    svg_item.resetTransform()
    svg_item.setTransform(transform)
    svg_item.scale(SCALE_FACTOR, -1 * SCALE_FACTOR)

    svg_item.setZValue(99)

    plot_item.addItem(svg_item, ignoreBounds=True)

    volc_labels = []
    volc_symbols = []
    for volcano in VOLCANOS:
        volc_lat = volcano['latitude']
        volc_lon = volcano['longitude']
        volcx, volcy = MERC_TRANSFORMER.transform(volc_lon, volc_lat)

        volc_name = volcano['name']

        anchor_point = volcano.get('anchor', (0, 0))
        volc_label = pg.TextItem(volc_name, (0, 0, 0), anchor=anchor_point)
        volc_label.setPos(volcx, volcy)
        volc_label.setZValue(100)
        volc_labels.append(volc_label)

        volc_symbol = plot_item.plot([volcx], [volcy],
                                     pen=None, name=volc_name,
                                     symbolPen=(0, 0, 0),
                                     symbol='t1',
                                     symbolBrush=None)
        volc_symbol.setZValue(100)
        volc_symbols.append(volc_symbol)

    return (plot_item, scale_widget, disp_widget, volc_labels)


_process_data = None


def _volcview_upload(img, sector, band, data_type, file_date):
    request_headers = {'username': 'internalavo',
                       'password': 'volcs4avo', }
    request_data = {
        'sector': sector['sector'],
        'band': band,
        'dataType': data_type,
        'imageUnixtime': file_date.timestamp(),
    }

    file_bytes = QByteArray()
    file_buffer = QBuffer(file_bytes)
    file_buffer.open(QIODevice.WriteOnly)
    img.save(file_buffer, "PNG")
    file_buffer.close()

    file_stream = BytesIO(file_bytes)

    filename = f"{band}-{data_type}-{file_date.timestamp()}.png"
    print(filename)
    print(request_data)
    file = {'file': (filename, file_stream)}

    request_url = 'https://avo-volcview.wr.usgs.gov/vv-api/imageApi/uploadImage'
    # requests.post(request_url, files=file, data=request_data, headers=request_headers)


def init_image_generation(img_sectors, file, height_list, file_date):
    """Generate one or more SO2 images, saving them in the specified output
    directory
    """
    if not isinstance(img_sectors, (list, tuple, dict)):
        raise TypeError(f"img_sectors must be a list of sectors or a single "
                        f"sector dict, not {type(img_sectors)}")

    # If a single sector was passed in, make it into a list
    if isinstance(img_sectors, dict):
        img_sectors = (img_sectors, )

    pg.setConfigOptions(background='w', foreground='k')

    if not isinstance(height_list, (list, tuple)):
        height_list = (height_list, )

    global _process_data

    for height in height_list:
        try:
            del _process_data
        except NameError:
            pass  # No data to delete

        gc.collect()  # clean up any prior data before loading new

        out_filename = f"so2_{height}.png"
        # Load data for the entire area
        lats = (40, 70)
        lons = (-210, -140)
        print(f"Beginning data load for {file} {height}")

        try:
            _process_data, coda_file = load_sector_data(lats, lons, file, height)
        except TypeError:
            print(f"No data found for {file}, {height}")
            continue

        del coda_file  # Get rid of no-longer needed references to the data
        # that might cause extra memory usage the next time through (holding
        # on to the previous data set while loading the new one).

        print(f"Data load for {file}, {height} complete")
        if _process_data is None or not _process_data['latitude'].any():
            # No data for this set of parameters. Try the next
            print(f"No data found for {file}, {height}")
            continue

        with numpy.errstate(invalid='ignore'):
            # Filter on density again so that any bins that wound up without data are removed.
            post_filter = _process_data['SO2_column_number_density'] >= 0

        for item in _process_data:
            try:
                if _process_data[item].shape[0] == post_filter.shape[0]:
                    _process_data[item] = _process_data[item][post_filter]
            except:
                continue

        # Process sectors in parallel, but only one period/height at a time so
        # we aren't trying to load too much data into memory
        sector_processes = []
        for sector in img_sectors:
            if len(sector_processes) >= mp.cpu_count():
                sector_processes[0].join()  # wait for the first one to be done
                del sector_processes[0]  # and delete it

            sector_process = mp.Process(target=_init_sector_generation,
                                        args=(sector, out_filename, file_date, height))

            sector_processes.append(sector_process)
            sector_process.start()

        for proc in sector_processes:
            proc.join()  # Wait for all sectors to be complete

        print(f"Image generation for {height} complete.")

    # Generate cloud and validity images
    # height will be whatever the last one was. Doesn't matter for cloud cover.
    try:
        _process_data, _ = load_sector_data(lats, lons, file, height, validity=0)
    except TypeError:
        print("No data found for cloud/validity load")
    else:
        out_filename = "cloud.png"
        sector_processes = []
        for sector in img_sectors:
            if len(sector_processes) >= mp.cpu_count():
                sector_processes[0].join()  # wait for the first one to be done
                del sector_processes[0]  # and delete it

            sector_process = mp.Process(target=_init_sector_generation,
                                        args=(sector, out_filename, file_date),
                                        kwargs={'gen_cloud': True, })

            sector_processes.append(sector_process)
            sector_process.start()

        for proc in sector_processes:
            proc.join()  # Wait for all sectors to be complete


def _init_sector_generation(sector, filename, file_date, height=None, gen_cloud=False):
    print(f"Generating image for {sector['name']}")
    # Create an output directory for this sector

    OUT_DIR = '/tmp/testVolcView'
    file_dir = os.path.realpath(os.path.join(OUT_DIR, f"{sector['name']}"))

    os.makedirs(file_dir, exist_ok=True)

    sector_lats = (sector['latFrom'], sector['latTo'])
    sector_lons = (sector['longFrom'], sector['longTo'])

    # Make sure we have some good data for this sector. There will probably be some nan values,
    # so ignore invalid
    with numpy.errstate(invalid='ignore'):
        if _process_data['latitude'].size <= 0 or not \
           (_process_data['SO2_column_number_density'] > 0).any():
            print(f"***No data loaded for {sector['name']}***")
            return False  # No data to work with for this sector

    show_volc_names = sector.get('showAllLabels', True)
    hide_all_names = sector.get('hideAllLabels', False)

    generate_sector_image(sector_lats, sector_lons, _process_data, file_dir,
                          show_volc_names, hide_all_names, filename, sector, file_date,
                          height, gen_cloud)

    print(f"Sector image generation complete for sector {sector['name']}")
    return True


def _gen_sector_bounds(sectors):
    for sector in sectors:
        pix_size = float(sector['pixelSize'])  # In km
        center_lat = float(sector['centerLat'])
        center_lon = float(sector['centerLng'])
        width = float(sector['imageWidth'])
        height = float(sector['imageHeight'])

        half_width_meters = (width / 2) * 1000 * pix_size
        half_height_meters = (height / 2) * 1000 * pix_size

        # latitude is easy
        lat_degrees_per_meter = 1 / 111131.745
        lat_half_degrees = half_height_meters * lat_degrees_per_meter

        # Longitude is a tad more complicated, as it is based on latitude.
        # Use the center value for latitude.
        lon_degrees_per_meter = 1 / (math.cos(math.radians(center_lat)) * 111321)
        lon_half_degrees = half_width_meters * lon_degrees_per_meter

        # Add keys to match what we are expecting for values.
        # Simper than changing our expectations! :)
        sector['latFrom'] = center_lat - lat_half_degrees
        sector['latTo'] = center_lat + lat_half_degrees
        sector['longFrom'] = center_lon + lon_half_degrees
        sector['longTo'] = center_lon - lon_half_degrees
        sector['name'] = sector['sectorLabel']
        sector['showAllLabels'] = False


def _main(FILE, file_date):
    start = time.time()

    FILE_NAME = FILE.split('/')[-1]
    if FILE_NAME[:4] == "S5P_":
        HEIGHTS = ['1km', '7km']
        BAND = 'TROPOMI'
    elif FILE_NAME[:4] == "OMPS":
        HEIGHTS = ['TRL', 'TRM']
        BAND = 'OMPS'

    _gen_sector_bounds(volcview_sectors)  # "converts" in-place.

    print("Generating images")
    init_image_generation(volcview_sectors, FILE, HEIGHTS, file_date)

    print("Completed run in", time.time() - start)


if __name__ == "__main__":
    from . import wingdbstub

    FILE = '/Volumes/Transfer/TROPOMIData/NRTI/2019-09-17/S5P_NRTI_L2__SO2____20190917T000803_20190917T001303_09982_01_010107_20190917T005751.nc'

    FILE_DATE = datetime(2019, 9, 17, 0, 8, 3)

    # FILE = '/Volumes/Transfer/OMPS/2019-10-23/OMPS-NPP_NMSO2-PCA-L2_v1.1_2019m1023t003114_o00001_2019m1023t005116.h5'
    # FILE_DATE = datetime(2019, 10, 23, 0, 31, 14)

    _main(FILE, FILE_DATE)
