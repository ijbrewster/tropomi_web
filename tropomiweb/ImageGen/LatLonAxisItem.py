"""
LatLonAxisItem.py - Subclass of PyQtGraph.AxisItem
Copyright © 2020 Alaska Volcano Observatory
Distributed under MIT license. See license.txt for more information
"""
import math

import pyproj
import pyqtgraph as pg


lat_lon_proj = pyproj.Proj('epsg:4326', preserve_units=False)
mercator_proj = pyproj.Proj(proj = 'merc', lon_0 = -90, preserve_units=False)

latlon_merc_transformer = pyproj.Transformer.from_proj(lat_lon_proj,
                                                       mercator_proj)


class LatLonAxisItem(pg.AxisItem):
    """Latitude/Longitude axis item"""
    inv_transformer = pyproj.Transformer.from_proj(mercator_proj,
                                                   lat_lon_proj)

    def tickStrings(self, values, scale, spacing):
        strings = []
        for value in values:
            if self.orientation in ['left', 'right']:
                # Calculate latitude
                lat, _ = self.inv_transformer.transform(0, value)
                val = round(lat)
            else:
                # Calculate longitude
                _, lon = self.inv_transformer.transform(value, 0)
                val = round(lon)

            strings.append(str(val))
        return strings

    def tickValues(self, minVal, maxVal, size):
        if self.orientation in ['top', 'bottom']:
            return super().tickValues(minVal, maxVal, size)

        # Longitude just works. Latitude we need to calculate which ticks to show
        minVal, maxVal = sorted((minVal, maxVal))

        tick_levels = self.tickSpacing(minVal, maxVal, size)
        all_values = []
        ticks = []
        for spacing, _ in tick_levels:
            if spacing is None:
                continue

            values = []

            # Find the starting value at this level
            # Figure the starting/stopping latitude values.
            min_lat, _ = self.inv_transformer.transform(0, minVal)
            max_lat, _ = self.inv_transformer.transform(0, maxVal)

            # Round to spacing value
            start_lat = math.ceil(min_lat / spacing) * spacing
            stop_lat = math.floor(max_lat / spacing) * spacing
            cur_lat = start_lat
            while cur_lat <= stop_lat:
                _, tick_val = latlon_merc_transformer.transform(cur_lat, 0)
                # Figure out which "bucket" to put this tick in
                if tick_val not in all_values:
                    all_values.append(tick_val)
                    values.append(tick_val)
                cur_lat += spacing
            ticks.append((spacing, values))

        return ticks
