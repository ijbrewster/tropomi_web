from celery import Celery

CELERYD_FORCE_EXECV = True

celery_app = Celery('tropomiweb',
                    broker='amqp://tropomiweb:Tr0p0m1W3b@localhost/tropomi',
                    backend='rpc://tropomiweb:Tr0p0m1W3b@localhost/tropomi',
                    include=['tropomiweb.ImageGen.GenerateSectorImages'],
                    task_serializer='pickle')

celery_app.conf.task_serializer = 'pickle'
celery_app.conf.result_serializer = 'pickle'
celery_app.conf.accept_content = ['pickle']
celery_app.conf.task_track_started = True

if __name__ == '__main__':
    celery_app.start()

# rabbitmqctl add_user tropomiweb Tr0p0m1W3b
# rabbitmqctl add_vhost tropomi
# rabbitmqctl set_permissions -p tropomi tropomiweb ".*" ".*" ".*"
