import gc
import glob
import math
import os
import pickle
import traceback
import blosc

from datetime import datetime, timedelta

import pyproj
import pytz
import psycopg2
from psycopg2 import extras

from h5pyimport import NetCDFFile
from config import DATA_DIR, OMPS_DIR, DEFAULT_SECTORS, IMG_SIZE

import wingdbstub

lat_lon_proj = pyproj.Proj(init='epsg:4326', preserve_units=False)
mercator_proj = pyproj.Proj(init='epsg:3857 +lon_0=-90', preserve_units=False)
merc_transformer = pyproj.Transformer.from_proj(lat_lon_proj, mercator_proj)


def load_period_data(lats, lons, files, height, validity=None, cloud_fraction=None, density=None,
                     start=None, stop=None):
    lat_from, lat_to = lats
    lon_from, lon_to = lons

    filters = [f"latitude>={lat_from-2}",
               f"latitude<={lat_to+2}",
               f"longitude_range({lon_from-2},{lon_to+2})",
               "valid(SO2_column_number_density)",
               ]

    if start is not None:
        filters.append(f"datetime_start>={start}")
    if stop is not None:
        filters.append(f"datetime_start<={stop}")

    filter_string = ";".join(filters)

    options = f'so2_column={height}'

    try:
        so2data_file = NetCDFFile(files)
        data = so2data_file.import_product(filter_string, options)
    except Exception:
        print(f"*****Got error when importing {height} product******")
        print(traceback.print_exc())
        return None

    if not data:
        data = None

    return (data, so2data_file)


if __name__ == "__main__":
    height_list = ['1km', '7km']

    all_files = glob.glob(DATA_DIR)
    all_files.sort()  # Make sure

    if DATA_DIR == OMPS_DIR:
        file_suffix = '.h5'
        file_type = 'OMPS'
    else:
        file_suffix = '.nc'
        file_type = "TROPOMI"

    current_date = datetime.today()
    tz = pytz.timezone("US/Alaska")
    day_stop = tz.localize(datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)).astimezone(pytz.utc)
    day_start = day_stop - timedelta(hours=24)

    seven_days = current_date - timedelta(days=7)
    thirty_days = current_date - timedelta(days=30)
    year = current_date - timedelta(days=365)

    days7_start = seven_days.strftime("%Y-%m-%d")
    days30_start = thirty_days.strftime("%Y-%m-%d")
    days365_start = year.strftime("%Y-%m-%d")

    idx_7 = None
    idx_30 = None
    idx_365 = None

    for idx, dir_name in enumerate(all_files):
        dir_date = dir_name.split('/')[-1]

        if idx_365 is None and dir_date >= days365_start:
            idx_365 = idx

        if idx_30 is None and dir_date >= days30_start:
            idx_30 = idx

        if idx_7 is None and dir_date >= days7_start:
            idx_7 = idx
            break

    file_lists = {
        'day': [f"{x}/*{file_suffix}" for x in all_files[-2:]],
        'week': [f"{x}/*{file_suffix}" for x in all_files[idx_7:]],
        'month': [f"{x}/*{file_suffix}" for x in all_files[idx_30:]],
        'year': [f"{x}/*{file_suffix}" for x in all_files[idx_365:]],
    }

    for period in ('day', 'week', 'month', 'year'):
        run_files = file_lists[period]
        file_start = run_files[0].split('/')[-2]
        file_end = run_files[-1].split('/')[-2]

        friendly_file_start = datetime.strptime(file_start, "%Y-%m-%d")
        friendly_file_start = friendly_file_start.strftime("%m/%d/%y")
        friendly_file_end = datetime.strptime(file_end, "%Y-%m-%d")
        friendly_file_end = friendly_file_end.strftime("%m/%d/%y")

        for height in height_list:
            output_file = f"tropomi_{period}_{height}.stack"
            lats = (40, 70)
            lons = (-210, -140)
            print(f"Beginning data load for {period}, {height}")

            try:
                if period == 'day':
                    # For the day period, we filter on start/stop time.
                    # Otherwise we just use calendar date.
                    _process_data, coda_file = load_period_data(lats, lons, run_files, height,
                                                                start=day_start.timestamp(),
                                                                stop=day_stop.timestamp())

                else:
                    _process_data, coda_file = load_period_data(lats, lons, run_files, height)
            except TypeError:
                print(f"No data found for {period}, {height}")
                continue

            if _process_data is None or not \
               _process_data['latitude'].any():
                # No data for this set of parameters. Try the next
                print(f"No data found for {period}, {height}")
                continue

            _process_idxs = coda_file._file_idxs
            del coda_file  # Get rid of no-longer needed references to the data that might cause
            # extra memory usage the next time through (holding on to the previous
            # data set while loading the new one).

            print(f"Data load for {period}, {height} complete")

            # Get sectors from DB
            try:
                conn = psycopg2.connect(host='akutan.snap.uaf.edu', database='so2data', user='israel')
            except psycopg2.OperationalError:
                print("Using sectors from config file")
                pass  # Default sectors remains as imported from config
            else:
                cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
                cur.execute('''SELECT
                    id,name,
                    latfrom::float "latFrom",
                    latto::float "latTo",
                    longfrom::float "longFrom",
                    longto::float "longTo",
                    showalllabels "showAllLabels",
                    hidealllabels "hideAllLabels"
                FROM sectors
                WHERE name='Hawaii' ''')
                print("Using sectors from DB")
                DEFAULT_SECTORS = cur.fetchall()
                conn.close()

            for sector in DEFAULT_SECTORS:
                print(f"Generating sector {sector['name']}")
                file_dir = os.path.realpath(os.path.join('/data/prestack_data/',
                                                         sector['name']))
                os.makedirs(file_dir, exist_ok=True)

                sector_lats = (sector['latFrom'], sector['latTo'])
                sector_lons = (sector['longFrom'], sector['longTo'])

                lat_from, lat_to = sector_lats
                lon_from, lon_to = sector_lons

                # Figure out binning for this sector
                # Bin sizes are in meters
                if DATA_DIR == OMPS_DIR:
                    y_increment = 22264
                    x_increment = 11132
                else:
                    y_increment = 2226.4
                    x_increment = 2226.4

                # lat_from is actually the "end" latitude for purposes of drawing the image,
                # so if the window resizes to large, you get a border at the bottom, such that
                # we need to change lat_from to fill the border, not lat_to.
                x_from, y_from = merc_transformer.transform(lon_from, lat_from - .5)
                x_to, y_to = merc_transformer.transform(lon_to + .25, lat_to)
                x_from = math.floor(x_from)

                num_lat = math.ceil((y_to - y_from) / y_increment)
                # Make sure we have enough increments. Should be close, but rounding may make it off a bit.
                while y_from + (y_increment * num_lat) <= y_to:
                    print("Not enough lat! Increasing by one!")
                    num_lat += 1

                num_lon = math.ceil((x_to - x_from) / x_increment)
                while x_from + (x_increment * num_lon) <= x_to:
                    print("Not enough lon! Increasing by one!")
                    num_lon += 1

                # Reduce resolution on large sectors
                if num_lat > (IMG_SIZE.height() / 3):
                    # Number of latitudes should be no more than about 1/3 the height.
                    num_lat = math.ceil(IMG_SIZE.height() / 3)
                    y_increment = (y_to - y_from) / (num_lat - 6)
                    passes = 0

                    # Make sure that the new increment size and number of increments
                    # actually gets us to the desired end point
                    while (y_from + (y_increment * num_lat)) <= y_to:
                        print("Scaled LAT too small! Incrementing", passes, "times.", (y_from + (y_increment * num_lat)), y_to)
                        num_lat += 1  # Add one more increment to (hopefully) push us over the threshold
                        passes += 1

                if num_lon > (IMG_SIZE.width() / 3):
                    num_lon = math.ceil(IMG_SIZE.width() / 3)
                    x_increment = (x_to - x_from) / (num_lon - 6)
                    while (x_from + (x_increment * num_lon)) <= x_to:
                        print("Scaled LON too small!")
                        num_lon += 1  # Add one more increment to push us over the threshold

                sector_file = _CodaFile()
                sector_file._file_data = _process_data
                sector_file._file_idxs = _process_idxs

                sector_file._bin_spatial(num_lat, y_from, y_increment,
                                         num_lon, x_from, x_increment,
                                         {'proj': 'merc', 'lon_0': '-90',
                                          'preserve_units': False})
                sector_data = sector_file._file_data

                del sector_file
                gc.collect()

                # We only need to keep one copy of latitude/longitude and bounds, since they are the same on all slices
                for key in ['latitude', 'longitude', 'latitude_bounds', 'longitude_bounds']:
                    sector_data[key] = sector_data[key][0:1]

                out_struct = {
                    'num_lat': num_lat,
                    'y_from': y_from,
                    'y_increment': y_increment,
                    'num_lon': num_lon,
                    'x_from': x_from,
                    'x_increment': x_increment,
                    'data': sector_data}

                print("Writing output file...")
                with open(os.path.join(file_dir, output_file), 'wb') as sector_file:
                    for key, value in out_struct.items():
                        if key == 'data':  # Handle data seperately
                            continue

                        pickle.dump((key, value), sector_file)
                    pickle.dump(None, sector_file)
                    for key, value in out_struct['data'].items():
                        c = blosc.compress_ptr(value.__array_interface__['data'][0],
                                               value.size, value.dtype.itemsize,
                                               clevel=3, cname='zstd',
                                               shuffle=blosc.SHUFFLE)
                        pickle.dump((key, value.dtype, value.shape, len(c)), sector_file)
                        sector_file.write(c)
