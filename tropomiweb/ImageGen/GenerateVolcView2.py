
import gc
import json
import math
import multiprocessing as mp
import os
import pytz
import time
import sys
import traceback
import warnings
from datetime import datetime
from io import BytesIO

import numpy
import pyproj
import requests

from PySide2.QtGui import (QPainterPath,
                           QFont,
                           QPen,
                           QTransform)

from PySide2.QtSvg import QGraphicsSvgItem

from PySide2.QtWidgets import (QApplication,
                               QWidget,
                               QVBoxLayout)

from PySide2.QtCore import (Qt, QSize, QByteArray, QBuffer, QIODevice)

import pyqtgraph as pg

from .GradientScale import GradientWidget
from .config import (VOLCANOS, MAP_IMG)
from .LatLonAxisItem import LatLonAxisItem
from .h5pyimport import NetCDFFile

volcview_sectors = [
    {'sectorGroup': '1 km sectors',
     'sectorLabel': 'Alaska Peninsula',
     'sector': '1kmAKAP',
     'centerLat': '56.00000',
     'centerLng': '-160.00000',
     'pixelSize': '1.00',
     'imageHeight': '800',
     'imageWidth': '1000',
     'sectorId': '32',
     },
    {'sectorGroup': '1 km sectors',
     'sectorLabel': 'Eastern Aleutians',
     'sector': '1kmAKEA',
     'centerLat': '53.00000',
     'centerLng': '-168.00000',
     'pixelSize': '1.00',
     'imageHeight': '800',
     'imageWidth': '1000',
     'sectorId': '21',
     },
    {'sectorGroup': '1 km sectors',
     'sectorLabel': 'South-Central Alaska',
     'sector': '1kmAKSC',
     'centerLat': '60.00000',
     'centerLng': '-150.00000',
     'pixelSize': '1.00',
     'imageHeight': '800',
     'imageWidth': '1000',
     'sectorId': '30',
     },
    {'sectorGroup': '1 km sectors',
     'sectorLabel': 'Western Aleutians',
     'sector': '1kmAKWA',
     'centerLat': '52.00000',
     'centerLng': '-180.00000',
     'pixelSize': '1.00',
     'imageHeight': '800',
     'imageWidth': '1000',
     'sectorId': '34',
     },
    {'sectorGroup': '1 km sectors',
     'sectorLabel': 'Kamchatka Peninsula',
     'sector': '1kmRUKA',
     'centerLat': '54.00000',
     'centerLng': '160.00000',
     'pixelSize': '1.25',
     'imageHeight': '800',
     'imageWidth': '1000',
     'sectorId': '39',
     }
]


def _generate_path(coord):
    path = QPainterPath()
    path.moveTo(*coord[0])
    path.lineTo(*coord[1])
    path.lineTo(*coord[2])
    path.lineTo(*coord[3])
    path.closeSubpath()
    return path


def _gen_sector_bounds(sectors):
    for sector in sectors:
        pix_size = float(sector['pixelSize'])  # In km
        center_lat = float(sector['centerLat'])
        center_lon = float(sector['centerLng'])
        width = float(sector['imageWidth'])
        height = float(sector['imageHeight'])

        half_width_meters = (width / 2) * 1000 * pix_size
        half_height_meters = (height / 2) * 1000 * pix_size

        # latitude is easy
        lat_degrees_per_meter = 1 / 111131.745
        lat_half_degrees = half_height_meters * lat_degrees_per_meter

        # Longitude is a tad more complicated, as it is based on latitude.
        # Use the center value for latitude.
        lon_degrees_per_meter = 1 / (math.cos(math.radians(center_lat)) * 111321)
        lon_half_degrees = half_width_meters * lon_degrees_per_meter

        # Add keys to match what we are expecting for values.
        # Simper than changing our expectations! :)
        sector['latFrom'] = center_lat - lat_half_degrees
        sector['latTo'] = center_lat + lat_half_degrees
        sector['longFrom'] = center_lon + lon_half_degrees
        sector['longTo'] = center_lon - lon_half_degrees
        sector['name'] = sector['sectorLabel']
        sector['showAllLabels'] = False


LAT_LON_PROJ = pyproj.Proj(init='epsg:4326', preserve_units=False)
MERCATOR_PROJ = pyproj.Proj(init='epsg:3857 +lon_0=-90', preserve_units=False)
MERC_TRANSFORMER = pyproj.Transformer.from_proj(LAT_LON_PROJ, MERCATOR_PROJ)


def _initalize_image_widgets():
    """Set up the various QT widgets used to display the plot"""
    # Set up display widgets
    try:
        QApplication(sys.argv + ['-platform', 'offscreen'])  # So we can make widgets :)
    except RuntimeError as err:
        if "singleton" not in str(err):
            raise

    font = QFont("Arial", 10)
    scale_font = QFont("Arial", 11)

    disp_widget = QWidget()

    v_layout = QVBoxLayout()
    v_layout.setStretch(1, 0)
    v_layout.setContentsMargins(0, 0, 0, 5)
    v_layout.setSpacing(0)

    view_box = pg.ViewBox(border={'color': "000", 'width': 1},
                          lockAspect=1)
    # Set up the custom axis items that translate x,y mercator coordinates to lat lon.
    lon_spacing = [(1113194.907932737, 0),
                   (222638.98158654757, 0),
                   (111319.49079327285, 0)
                   ]

    lat_spacing = [(10, 0),
                   (2, 0),
                   (1, 0)
                   ]

    grid_pen = QPen(Qt.SolidLine)

    right_axis = LatLonAxisItem("right", pen=grid_pen)
    right_axis.setTickSpacing(levels=lat_spacing)
    right_axis.setGrid(38)
    right_axis.setTickFont(font)

    top_axis = LatLonAxisItem("top", pen=grid_pen)
    top_axis.setGrid(38)
    top_axis.setTickSpacing(levels=lon_spacing)
    top_axis.setTickFont(font)

    left_axis = LatLonAxisItem("left")
    left_axis.setTickSpacing(levels=lat_spacing)
    left_axis.setTickFont(font)

    bottom_axis = LatLonAxisItem("bottom")
    bottom_axis.setTickSpacing(levels=lon_spacing)
    bottom_axis.setTickFont(font)

    plot_widget = pg.PlotWidget(disp_widget,
                                axisItems={"right": right_axis,
                                           'top': top_axis,
                                           'left': left_axis,
                                           'bottom': bottom_axis,
                                           },
                                viewBox=view_box)

    top_axis.setZValue(95)
    right_axis.setZValue(95)

    scale_widget = GradientWidget(disp_widget)
    scale_widget.setOrientation("Horizontal")
    scale_widget.setFont(scale_font)
    scale_widget.setStyleSheet("background-color:white;")

    v_layout.addWidget(plot_widget)
    v_layout.addWidget(scale_widget)

    disp_widget.setLayout(v_layout)

    plot_item = plot_widget.getPlotItem()

    plot_item.showAxis('right')
    plot_item.showAxis('top')
    plot_item.hideButtons()

    svg_item = QGraphicsSvgItem(MAP_IMG)

    # These values are the limits for the mercator projection, +/-180 degree longitude.
    # No magic here:-)
    x_start = -20037508.34278924
    x_stop = 20037508.342789244
    x_size = x_stop - x_start

    # 85 degrees north
    y_stop = 19971868.880408563

    transform = QTransform()

    # This, however, IS a magic number, just to get things to line up properly north-south.
    # Probably a mis-configuration of the viewBox parameter for the SVG.
    transform.translate(x_start, y_stop - 2000)

    svg_width = svg_item.boundingRect().width()
    scale_factor = x_size / svg_width

    svg_item.resetTransform()
    svg_item.setTransform(transform)
    svg_item.scale(scale_factor, -1 * scale_factor)

    svg_item.setZValue(99)

    plot_item.addItem(svg_item, ignoreBounds=True)

    volc_labels = []
    volc_symbols = []
    for volcano in VOLCANOS:
        volc_lat = volcano['latitude']
        volc_lon = volcano['longitude']
        volcx, volcy = MERC_TRANSFORMER.transform(volc_lon, volc_lat)

        volc_name = volcano['name']

        anchor_point = volcano.get('anchor', (0, 0))
        volc_label = pg.TextItem(volc_name, (0, 0, 0), anchor=anchor_point)
        volc_label.setPos(volcx, volcy)
        volc_label.setZValue(100)
        volc_labels.append(volc_label)

        volc_symbol = plot_item.plot([volcx], [volcy],
                                     pen=None, name=volc_name,
                                     symbolPen=(0, 0, 0),
                                     symbol='t1',
                                     symbolBrush=None)
        volc_symbol.setZValue(100)
        volc_symbols.append(volc_symbol)

    return (plot_item, scale_widget, disp_widget, volc_labels)


def check_api(request_url):
    headers = {'Connection': 'close'}
    res = requests.get(request_url + "bandApi/all", headers=headers)
    bands = [band['band'] for band in res.json()]
    res = requests.get(request_url + "dataTypeApi/all", headers=headers)
    data_types = [x['dataType'] for x in res.json()]

    required_types = ['TROPOMI', 'OMPS']
    required_bands = ['LowTrop', 'MidTrop', 'Cloud']
    headers = {'username': 'israel',
               'password': 'brewster', }

    for dtype in required_types:
        if dtype.lower() not in data_types:
            request = {'label': dtype,
                       'dataType': dtype.lower(), }
            res = requests.post(request_url + 'dataTypeApi/dataType',
                                data=json.dumps(request),
                                headers=headers)
            print(res.status_code, res.text)

    for band in required_bands:
        if band not in bands:
            request = {'label': band,
                       'band': band, }
            res = requests.post(request_url + 'bandApi/band',
                                data=json.dumps(request),
                                headers=headers)
            print(res.status_code, res.text)


class DataFile:
    _data = None
    _du_val = None
    _normalized_du = None
    _bands = ('LowTrop', 'MidTrop')
    # request_url =
    # 'https://avo-volcview.wr.usgs.gov/vv-api/imageApi/uploadImage'
    _request_url = 'https://binarycascade.com/projects/vv-api/'
    _upload_path = 'imageApi/uploadImage'

    def __init__(self, data_file, file_date, sectors):
        # Check some values
        if not isinstance(sectors, (list, tuple, dict)):
            raise TypeError(f"img_sectors must be a list of sectors or a single "
                            f"sector dict, not {type(sectors)}")

        # If a single sector was passed in, make it into a list
        if isinstance(sectors, dict):
            sectors = (sectors, )

        # File to generate images for
        self._file = data_file
        # Date/time of this file
        self._file_date = file_date
        # Sectors to generate images for
        self._sectors = sectors

        # Figure out the file type
        self._file_name = data_file.split('/')[-1]
        if self._file_name[:4] == "S5P_":
            self._heights = ['1km', '7km']
            self._data_type = 'TROPOMI'
        elif self._file_name[:4] == "OMPS":
            self._heights = ['TRL', 'TRM']
            self._data_type = 'OMPS'

        # Initalize some constants
        pg.setConfigOptions(background='w', foreground='k')
        self._du_color_map = pg.ColorMap([0, .05, .1, .175, .25, .99, 1],
                                         [(255, 255, 255),
                                          (241, 187, 252),
                                          (53, 248, 244),
                                          (255, 225, 0),
                                          (248, 152, 6),
                                          (255, 19, 0),
                                          (255, 0, 0)])
        self._du_scale_labels = {0: "0 DU", .05: "1", .1: "2", .25: "5", .6: "12", 1: ">20 DU", }

        self._cloud_color_map = pg.ColorMap([0, 1], [(255, 255, 255), (0, 0, 0)])
        self._cloud_scale_labels = {0: "0%", 1: "100%", }

    def process_data(self):
        for idx, height in enumerate(self._heights):
            try:
                del self._data
            except AttributeError:
                pass
            else:
                gc.collect()

            print(f"Beginning data load for {self._file_name} {height}")

            try:
                self._load_data(height)
            except TypeError:
                print(f"No data found for {self._file_name}, {height}")
                continue

            if self._data is None or not self._data['latitude'].any():
                # No data for this set of parameters. Try the next
                print(f"No data found for {self._file_name}, {height}")
                continue

            # Process sectors in parallel, but only one period/height at a time so
            # we aren't trying to load too much data into memory
            self._run_processes(self._bands[idx])

            print(f"Image generation for {height} complete.")

        # We need to do a different load to calculate cloud cover
        try:
            self._load_data(validity=0, cloud_fraction='>=0')
        except TypeError:
            print("No data found for cloud load")
        else:
            self._run_processes(gen_cloud=True)

    def _run_processes(self, *args, **kwargs):
        sector_processes = []
        for sector in self._sectors:
            # self._generate_sector(sector, *args, **kwargs)
            if len(sector_processes) >= mp.cpu_count():
                sector_processes[0].join()  # wait for the first one to be done
                del sector_processes[0]  # and delete it

            sector_process = mp.Process(target=self._generate_sector,
                                        args=(sector, ) + args,
                                        kwargs=kwargs)

            sector_processes.append(sector_process)
            sector_process.start()

        for proc in sector_processes:
            proc.join()  # Wait for all sectors to be complete

    def _load_data(self, height=None, validity=1, **kwargs):
        filters = ["sensor_zenith_angle<62",
                   "solar_zenith_angle<70",
                   ]

        if validity is not None:
            filters.append(f"SO2_column_number_density_validity>={validity}")
        else:
            filters.append("valid(SO2_column_number_density)",)

        for arg, value in kwargs.items():
            filters.append(f"{arg}{value}")

        filter_string = ";".join(filters)

        options = f'so2_column={height}' if height else ''
        try:
            so2data_file = NetCDFFile(self._file)
            self._data = so2data_file.import_data(filter_string, options)
        except Exception:
            print(f"*****Got error when importing {height} product******")
            print(traceback.print_exc())
        finally:
            del so2data_file

    def _generate_sector(self, sector, band=None, gen_cloud=False):
        print(f"Generating image for {sector['name']}")

        lat_from, lat_to = (sector['latFrom'], sector['latTo'])
        lon_from, lon_to = (sector['longFrom'], sector['longTo'])

        # Make sure our longitude is in the "REAL" range
        if lon_from < -180:
            filt_lon_from = lon_from + 360
        elif lon_from > 180:
            filt_lon_from = lon_from - 360
        else:
            filt_lon_from = lon_from

        if lon_to > 180:
            filt_lon_to = lon_to - 360
        elif lon_to < -180:
            filt_lon_to = lon_to + 360
        else:
            filt_lon_to = lon_to

        # Just look at *this* sector
        with numpy.errstate(invalid='ignore'):
            filter_items = []
            # Filter on density again so that any bins that wound up without data are removed.
            filter_items.append(self._data['latitude'] >= lat_from - .5)
            filter_items.append(self._data['latitude'] <= lat_to + .5)

            lat_filter = numpy.logical_and(*filter_items)

            # Figure out longitude filter
            if filt_lon_to > filt_lon_from:
                filt_lon_from = [filt_lon_from, 180]
                filt_lon_to = [-180, filt_lon_to]
            else:
                filt_lon_from = [filt_lon_from]
                filt_lon_to = [filt_lon_to]

            lon_filters = []
            for start, stop in zip(filt_lon_from, filt_lon_to):
                lon_filters.append(numpy.logical_and(self._data['longitude'] <= start + .5, self._data['longitude'] >= stop - .5))

            if len(lon_filters) > 1:
                lon_filter = numpy.logical_or(*lon_filters)
            else:
                lon_filter = lon_filters[0]

            post_filter = numpy.logical_and(lat_filter, lon_filter)

            sector_data = {}

            for item in self._data:
                try:
                    if self._data[item].shape[0] == post_filter.shape[0]:
                        sector_data[item] = self._data[item][post_filter]
                    else:
                        sector_data[item] = self._data[item][:]
                except:
                    continue

        if not sector_data['latitude'].any():
            # No data for this set of parameters. Try the next
            print(f"No data found for {band}, {sector['sector']}")
            return

        self._du_val = sector_data['SO2_column_number_density'] * 2241.15  # Conversion Factor from manual
        self._normalized_du = self._du_val * (1 / 20)
        self._normalized_du[self._normalized_du > 1] = 1
        self._normalized_du[self._normalized_du < 0] = 0

        # When rounded to 5 digits, the color results are identical.
        self._normalized_du = numpy.round(self._normalized_du, 5)

        show_volc_names = sector.get('showAllLabels', True)
        hide_all_names = sector.get('hideAllLabels', False)

        pixel_bounds = numpy.stack((sector_data['latitude_bounds'],
                                    sector_data['longitude_bounds']),
                                   axis=-1)

        x_lat_lon = pixel_bounds[:, :, 1].reshape(pixel_bounds[:, :, 1].size)
        y_lat_lon = pixel_bounds[:, :, 0].reshape(pixel_bounds[:, :, 0].size)

        x_merc, y_merc = MERC_TRANSFORMER.transform(x_lat_lon, y_lat_lon)

        x_merc = x_merc.reshape(int(x_merc.size / 4), 4)
        y_merc = y_merc.reshape(int(y_merc.size / 4), 4)
        merc_pixel_bounds = numpy.stack([x_merc, y_merc], axis=2)

        # Generate path objects for each pixel for graphing purposes.
        # To get the shape of each pixel, shift each one to 0,0 lower left bounding box
        shifted_coords = merc_pixel_bounds - numpy.min(merc_pixel_bounds, axis=1)[:, None, :]
        # We have to do min twice to get the single min value for each group of corner points
        # If we only did it once, X and Y would be scaled seperately, distorting the shape.
        scale_factors = numpy.max(numpy.max(shifted_coords, axis=1), axis=1)
        # Scale each pixel to fit within -0.5 - +0.5
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            scaled_coords = (shifted_coords * (1 / scale_factors[:, None, None])) - .5
        # "Center" the scaled coordinates so the paths correctly represent the points
        scaled_coords -= (((numpy.max(scaled_coords, axis=1)
                            - numpy.min(scaled_coords, axis=1)) - 1) / 2)[:, None, :]

        pixel_paths = [_generate_path(x) for x in scaled_coords]

        calc_x, calc_y = MERC_TRANSFORMER.transform(sector_data['longitude'], sector_data['latitude'])

        x_from, y_from = MERC_TRANSFORMER.transform(lon_from, lat_from)
        x_to, y_to = MERC_TRANSFORMER.transform(lon_to, lat_to)
        x_range = [x_from, x_to]
        y_range = [y_from, y_to]

        # Make sure our x and y ranges give us the proper ratio to avoid stretching the image
        desired_ratio = 1.25  # width to height

        # <1 => height greater than width.
        # >1 => height less than width.
        width_height_ratio = abs(x_range[1] - x_range[0]) / abs(y_range[1] - y_range[0])

        if width_height_ratio < desired_ratio:
            # too tall for width. Add width to compensate.
            desired_width = abs(y_range[1] - y_range[0]) * desired_ratio
            actual_width = abs(x_range[1] - x_range[0])
            width_change = desired_width - actual_width
            # apply one-half of the difference to each end
            x_range[0] += width_change / 2
            x_range[1] -= width_change / 2
        elif width_height_ratio > desired_ratio:
            # too wide for height. Add height to compensate.
            desired_height = abs(x_range[1] - x_range[0]) / desired_ratio
            actual_height = abs(y_range[1] - y_range[0])
            height_change = desired_height - actual_height
            # apply one-half of the difference to each end
            x_range[0] -= height_change / 2
            x_range[1] += height_change / 2

        img_width = 1000
        img_height = 800

        view_size = QSize(img_width, img_height)

        plot_item, scale_widget, disp_widget, volc_labels = _initalize_image_widgets()

        for idx, volcano in enumerate(VOLCANOS):
            if (show_volc_names or (volcano.get('showAlways', False)) and not hide_all_names):
                label = volc_labels[idx]
                plot_item.addItem(label)

        view_widget = plot_item.getViewWidget()
        view_widget.parent().setFixedSize(view_size)
        view_widget.adjustSize()

        vbox = plot_item.getViewBox()
        vbox.setAspectLocked(True, 1)
        vbox.disableAutoRange()

        vbox.setRange(xRange=x_range, yRange=y_range, padding=0)

        ###############################################################
        # Plot Generation Code
        ###############################################################
        def plot_dataset(dataset, color_map, scale_labels):
            # Only generate the brush once for each unique value
            lookup_table = {x: pg.mkBrush(color_map.map(x)) for x in numpy.unique(dataset)}
            brushes = [lookup_table[x] for x in dataset]

            scale_widget.setGradient(color_map.getGradient())
            scale_widget.setLabels(scale_labels)

            # Generate Plot
            plot = plot_item.plot(calc_x, calc_y,
                                  pen=None,
                                  symbolPen=None,
                                  symbolBrush=brushes,
                                  pxMode=False,
                                  symbolSize=scale_factors,
                                  symbol=pixel_paths)

            plot.setZValue(-1e7)

            plot_item.getViewWidget().parent().grab()
            volcview_img = plot_item.getViewWidget().parent().grab()
            self._volcview_upload(volcview_img, sector, band)

            # DEBUG ####################
            dest_dir = f"/tmp/testVolcView/{sector['sector']}"
            os.makedirs(dest_dir, exist_ok=True)
            dest_file = f"{self._data_type}-{band}.png"
            dest_path = os.path.join(dest_dir, dest_file)
            volcview_img.save(dest_path)
            ###################

            plot_item.removeItem(plot)
        ###############################################################

        if not gen_cloud:
            plot_dataset(self._normalized_du, self._du_color_map, self._du_scale_labels)
        else:
            band = 'cloud'
            plot_dataset(sector_data['cloud_fraction'], self._cloud_color_map,
                         self._cloud_scale_labels)

    def _volcview_upload(self, img, sector, band):
        request_headers = {'username': 'israel',
                           'password': 'brewster', }
        request_data = {
            'sector': sector['sector'],
            'band': band,
            'dataType': self._data_type,
            'imageUnixtime': self._file_date.timestamp(),
        }

        file_bytes = QByteArray()
        file_buffer = QBuffer(file_bytes)
        file_buffer.open(QIODevice.WriteOnly)
        img.save(file_buffer, "PNG")
        file_buffer.close()

        file_stream = BytesIO(file_bytes)

        filename = f"{band}-{self._data_type}-{self._file_date.timestamp()}.png"
        file = {'file': (filename, file_stream)}

        res = requests.post(self._request_url + self._upload_path, files=file,
                            data=request_data, headers=request_headers)
        print(filename, res.status_code)


def main(data_file, file_date):
    start = time.time()
    # Convert volcview sector definitions to our "native" format
    _gen_sector_bounds(volcview_sectors)  # "converts" in-place.

    print("Generating images")
    # check_api('https://binarycascade.com/projects/vv-api/')

    file_processor = DataFile(data_file, file_date, volcview_sectors)
    file_processor.process_data()
    # init_image_generation(volcview_sectors, FILE, HEIGHTS, file_date)
    print("Completed run in", time.time() - start)


if __name__ == "__main__":
    from . import wingdbstub

    FILE = '/Volumes/Transfer/TROPOMIData/NRTI/2019-09-17/S5P_NRTI_L2__SO2____20190917T000803_20190917T001303_09982_01_010107_20190917T005751.nc'

    FILE_DATE = datetime(2019, 9, 17, 0, 8, 3, tzinfo=pytz.UTC)

    # FILE = '/Volumes/Transfer/OMPS/2019-10-23/OMPS-NPP_NMSO2-PCA-L2_v1.1_2019m1023t003114_o00001_2019m1023t005116.h5'
    # FILE_DATE = datetime(2019, 10, 23, 0, 31, 14, tzinfo=pytz.UTC)

    main(FILE, FILE_DATE)
