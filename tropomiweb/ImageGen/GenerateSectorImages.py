import argparse
import gc
import glob
import logging
import math
import multiprocessing
import os
import pathlib
import pickle
import queue
import sys
import time
import traceback
import warnings

from datetime import (datetime,
                      date,
                      timedelta)

from dateutil.parser import parse

import blosc
import numpy
import pyproj
import pytz
import psycopg2

# needed to make psycopg2.extras visible
from psycopg2 import extras

from PySide6.QtGui import (QPainterPath,
                           QFont,
                           QPen,
                           QBrush,
                           QTransform)

from PySide6.QtSvgWidgets import QGraphicsSvgItem

from PySide6.QtWidgets import (QApplication,
                               QWidget,
                               QVBoxLayout)

from PySide6.QtCore import (Qt, QBuffer)

from shapely.geometry import Polygon

import pyqtgraph as pg

from plotly.offline import plot
import plotly.graph_objs as go
import xarray

# This file may be run as a script, or imported from a module, so we need both import styles. Sigh.
try:
    from .GradientScale import GradientWidget
    from .config import *
    from .LatLonAxisItem import LatLonAxisItem
    from .celery_client import celery_app
    from .h5pyimport import NetCDFFile
except ImportError:
    from GradientScale import GradientWidget
    from config import *
    from LatLonAxisItem import LatLonAxisItem
    from celery_client import celery_app
    from h5pyimport import NetCDFFile
    
from zarr.errors import GroupNotFoundError

DEBUG = True


class AnchoredTextItem(pg.TextItem, pg.GraphicsWidgetAnchor):
    anchorItem = pg.GraphicsWidgetAnchor.anchor

    def __init__(self, text='', color=(200, 200, 200), html=None, anchor=(0, 0),
                 border=None, fill=None, angle=0, rotateAxis=None):
        super().__init__(text, color, html, anchor, border, fill, angle, rotateAxis)
        pg.GraphicsWidgetAnchor.__init__(self)


def flatten_lat_lon(data):
    lats = data['latitude_bounds']
    lons = data['longitude_bounds']

    # convert any out-of-bounds lons to correct values
    lons[lons < -180] = lons[lons < -180] + 360

    num_lat_repeat = lons.shape[0]
    # "Expand" lons and lats to 1D array of points
    lons = numpy.tile(lons, (lats.shape[0], 1))
    lats = numpy.repeat(lats, num_lat_repeat, 0)
    return (lons, lats)


def bounds_to_corners(lons, lats):
    lats = numpy.repeat(lats, 2, axis=1)
    lons = numpy.stack([lons, numpy.fliplr(lons)], axis=1).reshape(lons.shape[0], 4)
    return (lons, lats)


def flatten_data(data):
    """
        Take a N-dimensional data structure, and "flatten" it to a
        N-1 dimensional structure

        Parameters
        ----------
        data : dictionary
            A data dictonary, containing one or more data structures to be
            flattened

        Returns
        -------
        data : dictionary
            The dictionary of data, with the flattened data structures
    """
    # Flatten the x-y data into a simple time-series type data
    data = data.stack(time=["x", "y"])

    # Get the mean of the data across the "file" dimension
    try:
        data = data.mean('file')
    except ValueError:
        pass  # No file dimension - only one file, no need to average anything.

    # make sure the dimensions are in the correct order
    data = data.transpose('time', 'corners')

    return data


def _generate_path(coord):
    path = QPainterPath()
    path.moveTo(*coord[0])
    path.lineTo(*coord[1])
    path.lineTo(*coord[2])
    path.lineTo(*coord[3])
    path.closeSubpath()
    return path


def _calculate_area(shape_coords):
    """Shape should be four x,y pairs (in an equal area projection)
    defining the four corners of this area"""
    poly = Polygon(shape_coords)
    return poly.area


lat_lon_proj = pyproj.Proj('epsg:4326', preserve_units=False)
mercator_proj = pyproj.Proj(proj = 'merc', lon_0 = -90, preserve_units=False)
merc_transformer = pyproj.Transformer.from_proj(lat_lon_proj, mercator_proj)
laea_transformer = pyproj.Transformer.from_proj(mercator_proj, 'proj=laea')


def load_period_data(lats, lons, files, height, validity=None, cloud_fraction=None, density=None,
                     start_date=None, stop=None):
    lat_from, lat_to = lats
    lon_from, lon_to = lons

    filters = [f"latitude>={lat_from-2}",
               f"latitude<={lat_to+2}",
               f"longitude_range({lon_from-2},{lon_to+2})",
               "valid(SO2_column_number_density)",
               ]

    if start_date is not None:
        filters.append(f"datetime_start>={start_date}")
    if stop is not None:
        filters.append(f"datetime_start<={stop}")

    filter_string = ";".join(filters)

    options = f'so2_column={height}'

    try:
        so2data_file = NetCDFFile(files)
        logging.debug("Initialized loader. Begining load.")
        data = so2data_file.import_data(filter_string, options)
    except Exception:
        print(f"*****Got error when importing {height} product******")
        print(traceback.print_exc())
        return None

    if not data:
        data = None

    return (data, so2data_file)


def calc_time_series(data, out_dir, file_height):
    time_data = []
    so2_data = []

    # Figure out pixel areas. convert to x,y coordinates so we can use polygon math.
    lons = data['longitude_bounds'][0]
    lats = data['latitude_bounds'][0]

    x_laea, y_laea = laea_transformer.transform(lons.flatten(), lats.flatten())

    x_laea.shape = (int(x_laea.size / 4), 4)
    y_laea.shape = (int(y_laea.size / 4), 4)

    laea_pixel_bounds = numpy.stack((x_laea, y_laea), axis=2)

    pixel_areas = numpy.asarray([_calculate_area(x) for x in laea_pixel_bounds])  # area in meter square

    prev_date = None
    date_cnt = 0
    prev_sector = None
    density_data = sorted(zip(data['datetime_start'], data['SO2_column_number_density']),
                          key=lambda x: x[0])

    for point_time, so2_sector in density_data:
        # Translate the relative time to an absolute time
        point_time = datetime.fromtimestamp(point_time)
        point_time = point_time.date()
        if numpy.all(numpy.isnan(so2_sector)):
            continue  # No values in this slice. Move on.

        if point_time == prev_date:
            date_cnt += 1
            # This slice is for the same date as the last slice. Average together.
            with warnings.catch_warnings():
                # Probable that both slices with have NaN in the same slot on multiple occasions
                warnings.simplefilter("ignore")
                so2_sector = ((date_cnt * prev_sector) + so2_sector) / (date_cnt + 1)
                # so2_sector = numpy.nanmean(numpy.stack((so2_sector, prev_sector)), axis=0)
        else:
            date_cnt = 0

        # Save this sector as the previous one for averaging.
        prev_sector = so2_sector

        flat_density = so2_sector.flatten() * 64  # moles to grams
        masses = flat_density * pixel_areas  # g/m^2 * m^2 = g

        total_mass = numpy.nansum(masses) * 1e-9  # Kilo Tonnes

        if total_mass < 0:
            total_mass = 0

        if point_time == prev_date:
            # Just update the total mass for this date
            so2_data[-1] = total_mass
        else:
            # create a new date entry
            so2_data.append(total_mass)
            time_data.append(point_time)
            prev_date = point_time

    # Create a plot for the time series
    trace = go.Scattergl(x=time_data, y=so2_data, connectgaps=True)

    filename = f'timeseries-{file_height}.html'
    save_file = os.path.join(out_dir, filename)

    config = {
    }

    layout = dict(
        title=f'{file_height} SO<sub>2</sub> Mass',
        margin={
            "l": 60,
            "r": 10,
            "t": 10,
            "b": 30,
        },
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                         label='1m',
                         step='month',
                         stepmode='backward'),
                    dict(count=6,
                         label='6m',
                         step='month',
                         stepmode='backward'),
                    dict(step='all')
                ])
            ),
            rangeslider=dict(
                visible=False
            ),
            type='date'
        ),
        yaxis={
            "title": {
                "text": "Kilotonnes",
            },
        }
    )

    plot_div = plot({"data": [trace],
                     "layout": layout, },
                    include_plotlyjs=False, output_type="div", auto_open=False,
                    config=config)

    with open(save_file, 'w') as html_file:
        html_file.write(plot_div)


du_color_map = pg.ColorMap([0, .05, .1, .175, .25, .99, 1],
                           [(255, 255, 255),
                            (241, 187, 252),
                            (53, 248, 244),
                            (255, 225, 0),
                            (248, 152, 6),
                            (255, 19, 0),
                            (255, 0, 0)])


def generate_sector_image(lats, lons, data, out_dir, plot_item, scale_widget,
                          label_widget, file_period="", file_height="", validity=None,
                          cloud_fraction=None, density=None, filename=None):

    lat_from, lat_to = lats
    lon_from, lon_to = lons

    good_data = True
    with numpy.errstate(invalid='ignore'):
        if 'latitude' not in data or \
           data['latitude'].size <= 0 or not \
           (~numpy.isnan(data['SO2_column_number_density'])).any():
            good_data = False
        else:
            # Filter on density again so that any bins that wound up without data are removed.
            if density is not None:
                # post_filter = numpy.logical_and(~numpy.isnan(data['SO2_column_number_density']),
                # data['SO2_column_number_density'] >= density)
                post_filter = data['SO2_column_number_density'] >= density
            else:
                post_filter = ~numpy.isnan(data['SO2_column_number_density'])

    current_label = label_widget.textItem.toPlainText()

    # For some inexplicable reason, if we do this in the else clause, it crashes.
    # So do it here instead and simply override if needed.
    label_widget.setHtml(f"{current_label};<br><br><b>NO DATA FOR SECTOR/DATE RANGE</b>")

    if good_data:
        if not post_filter.all():
            data = data.where(post_filter, drop = True)

        # for item in data:
            # try:
            # if data[item].shape[0] == post_filter.shape[0]:
            # data[item] = data[item][post_filter]
            # except Exception:
            # continue

        pixel_bounds = numpy.stack((data['latitude_bounds'], data['longitude_bounds']),
                                   axis=-1)

        x_merc = pixel_bounds[:, :, 1].reshape(pixel_bounds[:, :, 1].size)
        y_merc = pixel_bounds[:, :, 0].reshape(pixel_bounds[:, :, 0].size)

        x_merc = x_merc.reshape(int(x_merc.size / 4), 4)
        y_merc = y_merc.reshape(int(y_merc.size / 4), 4)
        merc_pixel_bounds = numpy.stack([x_merc, y_merc], axis=2)

        DU = data['SO2_column_number_density'] * 2241.15  # Conversion Factor from manual

        x_laea, y_laea = laea_transformer.transform(x_merc, y_merc)
        x_laea = x_laea.reshape(int(x_laea.size / 4), 4)
        y_laea = y_laea.reshape(int(y_laea.size / 4), 4)
        laea_pixel_bounds = numpy.stack([x_laea, y_laea], axis=2)

        laea_areas = numpy.fromiter([_calculate_area(x) for x in laea_pixel_bounds],
                                    dtype=float)

        pixel_moles = laea_areas * data['SO2_column_number_density']
        pixel_mass = (pixel_moles) * 64  # in grams
        total_mass = pixel_mass.sum()

        mass_tonnes = total_mass * 1e-6
        suffix = "t"
        mass_kt = mass_tonnes / 1000

        if abs(mass_tonnes) > 1000:
            mass_tonnes /= 1000
            suffix = "kt"

        label_widget.setText(f"{current_label}")  # {round(mass_tonnes,2)}{suffix}")

        # Generate path objects for each pixel for graphing purposes.
        # To get the shape of each pixel, shift each one to 0,0 lower left bounding box
        shifted_coords = merc_pixel_bounds - numpy.min(merc_pixel_bounds, axis=1)[:, None, :]
        # We have to do min twice to get the single min value for each group of corner points
        # If we only did it once, X and Y would be scaled seperately, distorting the shape.
        scale_factors = numpy.max(numpy.max(shifted_coords, axis=1), axis=1)
        # Scale each pixel to fit within -0.5 - +0.5
        scaled_coords = (shifted_coords * (1 / scale_factors[:, None, None])) - .5
        # "Center" the scaled coordinates so the paths correctly represent the points
        scaled_coords -= (((numpy.max(scaled_coords, axis=1) -
                            numpy.min(scaled_coords, axis=1)) - 1) / 2)[:, None, :]

        pixel_paths = [_generate_path(x) for x in scaled_coords]

        calc_x, calc_y = merc_transformer.transform(data['latitude'], data['longitude'])

        # generate images for so2 density and cloud coverage
        # Generate SO2 brushes
        normalized_du = DU * (1 / 20)
        normalized_du[normalized_du > 1] = 1
        normalized_du[normalized_du < 0] = 0

        # When rounded to 5 digits, the color results are identical.
        normalized_du = numpy.round(normalized_du, 5)

        lookup_table = {x: pg.mkBrush(du_color_map.map(x)) for x in numpy.unique(normalized_du)}
        so2brushes = [lookup_table[x] for x in normalized_du.data]
        so2_plot = plot_item.plot(calc_x, calc_y,
                                  pen=None, name="SO2",
                                  symbolPen=None,
                                  symbolBrush=so2brushes,
                                  pxMode=False,
                                  symbolSize=scale_factors,
                                  symbol=pixel_paths)

        so2_plot.setZValue(-1e7)
    else:
        print(f"***No data loaded for {file_period} {file_height}***")
        mass_kt = None

    x_from, y_from = merc_transformer.transform(lat_from, lon_from)
    x_to, y_to = merc_transformer.transform(lat_to, lon_to)
    x_range = (math.ceil(x_from), math.floor(x_to))
    y_range = (math.ceil(y_from), math.floor(y_to))
    if DEBUG:
        print(f"View Box x_range: {x_range} y_range: {y_range}")

    width_height_ratio = abs(x_to - x_from) / abs(y_to - y_from)
    view_widget = plot_item.getViewWidget()
    img_width = 645
    MAX_HEIGHT = 750
    img_height = img_width / width_height_ratio
    if img_height > MAX_HEIGHT:
        img_height = MAX_HEIGHT
        img_width = img_height * width_height_ratio
        img_width = math.floor(img_width)

    img_height = math.floor(img_height)

    view_size = QSize(img_width, img_height)
    view_widget.setFixedSize(view_size)
    view_widget.parent().adjustSize()

    isinstance(plot_item, pg.PlotItem)
    vbox = plot_item.getViewBox()
    isinstance(vbox, pg.ViewBox)
    vbox.disableAutoRange()
    vbox.setRange(xRange=x_range, yRange=y_range, padding=0.0)

    if filename is None:
        filename = f"so2-{file_period}-{file_height}.png"

    scale_widget.setGradient(du_color_map.getGradient())
    scale_widget.setLabels({0: "0 DU", .05: "1", .1: "2", .25: "5", .6: "12", 1: ">20 DU", })

    dest_file = os.path.join(out_dir, filename)

    plot_item.getViewWidget().parent().grab()  # Hack just to get the scale to update properly. Thankfully this is fast.

    buffer = QBuffer()
    buffer.open(QBuffer.ReadWrite)

    # Save to memory and let Python save to disk to avoid crashing under certain circumstances.
    plot_item.getViewWidget().parent().grab().save(buffer, "PNG")
    buffer.seek(0)

    with open(dest_file, 'wb') as test_file:
        test_file.write(buffer.data().data())

    link_file = f"so2-{file_period}-{file_height}.png"
    link_path = os.path.join(out_dir, link_file)
    if file_period != 'custom' and link_file != filename:
        if os.path.islink(link_path):
            os.unlink(link_path)

        try:
            os.symlink(filename, link_path)
        except FileExistsError:
            pass  # There is something with this name, but not a link. Oh well. Move on.

    # plot_item.removeItem(so2_plot)

    return mass_kt


def _initalize_image_widgets(img_tag, lat_spacing=1, lon_spacing=111319.49079327285):
    """Set up the various Qt widgets needed to draw and display the graph. lat_spacing is in units
    of degrees, and will be calculated based on the latitude, since with the mercator projection
    latitude spacing increases as you move toward the poles. longitude spacing is even
    with the mercator projection, so lon_spacing is in units of meters."""
    # Set up display widgets
    try:
        app = QApplication(sys.argv + ['-platform', 'offscreen'])  # So we can make widgets :)
    except RuntimeError as err:
        if "singleton" not in str(err):
            print("Got some other error", err)
            raise

        app = QApplication.instance()

    font = QFont("Arial", 10)
    scale_font = QFont("Arial", 11)

    disp_widget = QWidget()
    disp_widget.setStyleSheet("background-color: rgba(255,255,255,255)")

    v_layout = QVBoxLayout()
    v_layout.setStretch(1, 0)
    v_layout.setContentsMargins(0, 0, 0, 0)
    v_layout.setSpacing(0)

    view_box = pg.ViewBox(border={'color': "#000", 'width': 1},
                          enableMenu=False,
                          enableMouse=False)

    view_box.setBackgroundColor('#EEE')

    lon_spacing = [(lon_spacing, 0)]
    lat_spacing = [(lat_spacing, 0)]

    grid_pen = QPen(Qt.SolidLine)

    right_axis = LatLonAxisItem("right", pen=grid_pen)
    right_axis.setTickSpacing(levels=lat_spacing)
    right_axis.setGrid(38)
    right_axis.setTickFont(font)

    top_axis = LatLonAxisItem("top", pen=grid_pen)
    top_axis.setGrid(38)
    top_axis.setTickSpacing(levels=lon_spacing)
    top_axis.setTickFont(font)

    left_axis = LatLonAxisItem("left")
    left_axis.setTickSpacing(levels=lat_spacing)
    left_axis.setTickFont(font)

    bottom_axis = LatLonAxisItem("bottom")
    bottom_axis.setTickSpacing(levels=lon_spacing)
    bottom_axis.setTickFont(font)

    plot_widget = pg.PlotWidget(disp_widget,
                                axisItems={"right": right_axis,
                                           'top': top_axis,
                                           'left': left_axis,
                                           'bottom': bottom_axis,
                                           },
                                viewBox=view_box)

    plot_widget.setBackgroundBrush(QBrush(Qt.transparent, Qt.NoBrush))

    top_axis.setZValue(95)
    right_axis.setZValue(95)

    scale_widget = GradientWidget(disp_widget)
    scale_widget.setOrientation("Horizontal")
    scale_widget.setFont(scale_font)
    scale_widget.setStyleSheet("background-color:white;")

    date_label = AnchoredTextItem(text=img_tag, color=(0, 0, 0))
    date_label.setParentItem(view_box)
    date_label.anchorItem(itemPos=(0, 0), parentPos=(0, 0), offset=(5, 5))

    date_label.setFont(font)

    v_layout.addWidget(plot_widget)
    v_layout.addWidget(scale_widget)

    disp_widget.setLayout(v_layout)

    plot_item = plot_widget.getPlotItem()

    plot_item.showAxis('right')
    plot_item.showAxis('top')
    plot_item.hideButtons()

    svg_item = QGraphicsSvgItem(MAP_IMG)

    # These values are the limits for the mercator projection, +/-180 degree longitude.
    # No magic here:-)
    x_start = -20037508.34278924
    x_stop = 20037508.342789244
    x_size = x_stop - x_start

    # 85 degrees north
    y_stop = 19971868.880408563

    transform = QTransform()

    # This, however, IS a magic number, just to get things to line up properly north-south.
    # Probably a mis-configuration of the viewBox parameter for the SVG.
    transform.translate(x_start, y_stop - 2000)

    svg_width = svg_item.boundingRect().width()
    SCALE_FACTOR = x_size / svg_width
    transform.scale(SCALE_FACTOR, -1 * SCALE_FACTOR)

    svg_item.resetTransform()
    svg_item.setTransform(transform)

    svg_item.setZValue(99)

    plot_item.addItem(svg_item, ignoreBounds=True)

    volc_labels = []
    volc_symbols = []
    for volcano in VOLCANOS:
        volc_lat = volcano['latitude']
        volc_lon = volcano['longitude']
        volcx, volcy = merc_transformer.transform(volc_lat, volc_lon)

        volc_name = volcano['name']

        anchor_point = volcano.get('anchor', (0, 0))
        volc_label = pg.TextItem(volc_name, (0, 0, 0), anchor=anchor_point)
        volc_label.setPos(volcx, volcy)
        volc_label.setZValue(100)
        volc_labels.append(volc_label)

        volc_symbol = plot_item.plot([volcx], [volcy],
                                     pen=None, name=volc_name,
                                     symbolPen=(0, 0, 0),
                                     symbol='t1',
                                     symbolBrush=None)
        volc_symbol.setZValue(100)
        volc_symbols.append(volc_symbol)

    return (plot_item, scale_widget, volc_labels, date_label, disp_widget, app)


@celery_app.task
def celery_init_image_generation(img_sectors, date_range=None,
                                 custom_filename=False, custom_height='1km',
                                 datadir=None):

    global DATA_DIR
    if datadir is not None:
        DATA_DIR = datadir

    init_image_generation(img_sectors, date_range, custom_filename, custom_height)


_process_data = None
_process_idxs = None


def _get_start_dates(period=None):
    current_date = datetime.today()
    tz = pytz.timezone("US/Alaska")
    day_stop = tz.localize(datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)).astimezone(pytz.utc)

    day_start = day_stop - timedelta(hours=24)
    seven_days = current_date - timedelta(days=7)
    thirty_days = current_date - timedelta(days=30)
    year = current_date - timedelta(days=365)

    if period == 'day':
        return day_start
    if period == 'week':
        return seven_days
    if period == 'month':
        return thirty_days
    if period == 'year':
        return year

    return (day_start, seven_days, thirty_days, year)


def init_image_generation(img_sectors, date_range=None, custom_filename=False, custom_height=None):
    """Generate one or more SO2 images, saving them in the specified output directory
    If date_range is specified, it must be a start,stop date/datetime tuple or list.
    """
    global OUT_DIR

    if not isinstance(img_sectors, (list, tuple, dict)):
        raise TypeError(f"img_sectors must be a list of sectors or a single sector dict, not {type(img_sectors)}")

    # If a single sector was passed in, make it into a list
    if isinstance(img_sectors, dict):
        img_sectors = (img_sectors, )

    if date_range is not None:
        if (not isinstance(date_range, (list, tuple)) or not
                isinstance(date_range[0], (date, datetime)) or not
                isinstance(date_range[1], (date, datetime))):
            print(date_range)
            raise TypeError(f"If specified, date range must be a (start,end) date/datetime tuple or list, not {type(date_range)}")

    pg.setConfigOptions(background='#EEE', foreground='k')

    if custom_filename:
        OUT_DIR = '/tmp/'

    all_files = glob.glob(DATA_DIR)
    all_files.sort()  # Make sure

    if DATA_DIR == OMPS_DIR:
        file_suffix = '.h5'
        file_type = 'OMPS'
    else:
        file_suffix = '.nc'
        file_type = "TROPOMI"

    if date_range is None:
        # Run for the default periods of 1 day, 7 days, 30 days, and 365 days
        height_list = custom_height if custom_height is not None else ['1km', '7km']
        if not isinstance(height_list, (list, tuple)):
            height_list = (height_list, )

        day_start, seven_days, thirty_days, year = _get_start_dates()

        days7_start = seven_days.strftime("%Y-%m-%d")
        days30_start = thirty_days.strftime("%Y-%m-%d")
        days365_start = year.strftime("%Y-%m-%d")

        idx_7 = None
        idx_30 = None
        idx_365 = None

        for idx, dir_name in enumerate(all_files):
            name_parts = dir_name.split('/')
            dir_date = f"{name_parts[-3]}-{name_parts[-2]}-{name_parts[-1]}"

            if idx_365 is None and dir_date >= days365_start:
                idx_365 = idx

            if idx_30 is None and dir_date >= days30_start:
                idx_30 = idx

            if idx_7 is None and dir_date >= days7_start:
                idx_7 = idx
                break

        file_lists = {
            'day': [f"{x}/*{file_suffix}" for x in all_files[-2:]],
            'week': [f"{x}/*{file_suffix}" for x in all_files[idx_7:]],
            'month': [f"{x}/*{file_suffix}" for x in all_files[idx_30:]],
            'year': [f"{x}/*{file_suffix}" for x in all_files[idx_365:]],
        }

        TS_SQL = """INSERT INTO ts
            (sector,
            type,
            altitude,
            record_date,
            record_value)
        VALUES
            (%(sector)s,
            %(type)s,
            %(altitude)s,
            %(record_date)s,
            %(record_value)s)
        ON CONFLICT (sector,record_date,type,altitude) DO UPDATE
        SET record_value=excluded.record_value"""

        # Generate week, month, and year images for 1km and 7km heights.
        periods = ('day', 'week', 'month', 'year') if DEBUG else ('day', 'week', 'month', 'year')
        for period in periods:
            run_files = file_lists[period]
            start_parts = run_files[0].split('/')
            end_parts = run_files[-1].split('/')
            file_start = f"{start_parts[-4]}-{start_parts[-3]}-{start_parts[-2]}"
            file_end = f"{end_parts[-4]}-{end_parts[-3]}-{end_parts[-2]}"
            
            # file_start = run_files[0].split('/')[-2]
            # file_end = run_files[-1].split('/')[-2]

            friendly_file_start = datetime.strptime(file_start, "%Y-%m-%d")
            friendly_file_start = friendly_file_start.strftime("%m/%d/%y")
            friendly_file_end = datetime.strptime(file_end, "%Y-%m-%d")
            friendly_file_end = friendly_file_end.strftime("%m/%d/%y")

            # Process sectors in parallel, but only one period/height at a time so we aren't trying
            # to load too much data into memory. Annual stacks can't be parallelized - too big.
            sector_processes = []
            max_processes = multiprocessing.cpu_count()  # if DATA_DIR != OMPS_DIR else 1
            if DATA_DIR != OMPS_DIR:
                if period == 'year':
                    max_processes = 1  # Don't try to do multiple annual stacks at once
                elif period == 'month':
                    max_processes /= 2

            # We are only interested in the sector mass for the day periods, so for the others
            # we don't need to bother with the queue
            res_queue = multiprocessing.Queue() if period == 'day' else None
            output_filename = f"so2_{period}_%s_{file_start}_{file_end}.png"
            img_tag = f'{friendly_file_start} - {friendly_file_end}; {file_type} %s'

            for sector in img_sectors:
                # Don't try to multiprocess if we are only doing a single sector
                if len(img_sectors) > 1 and max_processes > 1:
                    # Don't try to do more things at once than available cores
                    if len(sector_processes) >= max_processes:
                        sector_processes.pop(0).join()

                    sector_process = multiprocessing.Process(target=_init_sector_generation,
                                                             args=(sector, period, height_list,
                                                                   output_filename, img_tag,
                                                                   all_files, res_queue),
                                                             kwargs = {'datadir': DATA_DIR, },
                                                             daemon=False)
                    sector_process.start()

                    sector_processes.append(sector_process)
                else:
                    print("Using a single process")
                    _init_sector_generation(sector, period, height_list, output_filename,
                                            img_tag, all_files, res_queue)

            for proc in sector_processes:
                try:
                    proc.join()  # Wait for all sectors to be complete
                except multiprocessing.TimeoutError:
                    print("***Got timeout while waiting!", proc.is_alive())

            if period == 'day':
                while True:
                    try:
                        sector_id, mass_kt, height = res_queue.get_nowait()
                    except queue.Empty:
                        print("No values remaining")
                        break  # got all results
                    if mass_kt is not None:
                        dbargs = {'type': file_type,
                                  'record_date': day_start.date(),
                                  'record_value': mass_kt.item(),
                                  'sector': sector_id,
                                  'altitude': height}

                        try:
                            dbconn = psycopg2.connect(host='akutan.snap.uaf.edu', user='israel',
                                                      database='so2data')
                        except psycopg2.OperationalError:
                            continue

                        dbcur = dbconn.cursor()
                        dbcur.execute(TS_SQL, dbargs)
                        if DEBUG:  # If debugging, just make sure the query
                            # can run, don't save changes
                            dbconn.rollback()
                        else:
                            dbconn.commit()
                        dbconn.close()

                        print(f"Saved mass of {mass_kt.item()} for {day_start.date()} to db")

            print(f"Image generation for {period} complete")
    else:
        date_start, date_stop, *excluded = date_range
        date_start_l = date_start.strftime("%Y-%m-%d")
        date_stop_l = date_stop.strftime("%Y-%m-%d")
        idx_start = None
        idx_stop = None
        for idx, dir_name in enumerate(all_files):
            dir_date = dir_name.split('/')[-1]
            if idx_start is None and dir_date >= date_start_l:
                idx_start = idx
            if idx_stop is None and dir_date >= date_stop_l:
                idx_stop = idx
                break
        else:
            idx_stop = len(all_files)  # will wind up well beyond the end, but that's fine.

        custom_files = [f"{x}/*{file_suffix}" for x in all_files[idx_start:idx_stop + 1] if x.split('/')[-1] not in excluded]
        print("Generating image for custom")
        for sector in img_sectors:
            lats = (sector['latFrom'], sector['latTo'])
            lons = (sector['longFrom'], sector['longTo'])

            file_start = custom_files[0].split('/')[-2]
            file_start = datetime.strptime(file_start, "%Y-%m-%d").strftime("%m/%d/%y")
            file_end = custom_files[-1].split('/')[-2]
            file_end = datetime.strptime(file_end, "%Y-%m-%d").strftime("%m/%d/%y")
            img_tag = f'{file_start} - {file_end}; {custom_height}'
            _process_data, _ = load_period_data(lats, lons, custom_files, custom_height)

            if _process_data is None or not \
               _process_data['latitude'].any():
                # No data for this set of parameters. Try the next
                print(f"No data found for custom, {custom_height}")
                continue

            _init_sector_generation(sector, "custom", custom_height, custom_filename,
                                    img_tag, all_files, sector_count=len(img_sectors))


def _init_sector_generation(sector, period, levels, filename, img_tag, all_files,
                            result_queue=None, sector_count=1, manual_data=None,
                            manual_idxs=None, datadir = None):

    if datadir:
        global DATA_DIR
        DATA_DIR = datadir

    if manual_data or manual_idxs:
        global _process_data
        global _process_idxs

    if manual_data:
        _process_data = manual_data
        pg.setConfigOptions(background='w', foreground='k')

    if manual_idxs:
        _process_idxs = manual_idxs

    # Create an output directory for this sector
    if period == "custom":
        file_dir = OUT_DIR
        if sector_count > 1:
            file_dir = os.path.join(file_dir, sector['name'])
    else:
        file_dir = os.path.realpath(os.path.join(OUT_DIR, sector['name']))

    os.makedirs(file_dir, exist_ok=True)

    show_volc_names = sector.get('showAllLabels', True)
    hide_all_names = sector.get('hideAllLabels', False)

    sector_lats = (sector['latFrom'], sector['latTo'])
    sector_lons = (sector['longFrom'], sector['longTo'])

    lat_from, lat_to = sector_lats
    lon_from, lon_to = sector_lons

    # Latitude and longitude "mark" spacing for the scale bars on the graph.
    lon_diff = lon_to - lon_from
    max_lines = 13
    if lon_diff / 2 > max_lines:
        # 5 degree increments
        lat_spacing = 5
        lon_spacing = 557305.2572745753
    elif lon_diff > max_lines:
        # 2 degree increments
        lat_spacing = 2
        lon_spacing = 222684.20850554318
    else:
        # 1 degree increments
        lat_spacing = 1
        lon_spacing = 111325.14286638486

    flat_data = None
    for height in levels:
        print(f"Generating {period} {height} image for {sector['name']}")
        img_filename = filename % (height)
        tag = img_tag % (height)

        (plot_item,
         scale_widget,
         volc_labels,
         date_label,
         disp_widget,
         app) = _initalize_image_widgets(tag, lat_spacing, lon_spacing)

        for idx, volcano in enumerate(VOLCANOS):
            if (show_volc_names or (volcano.get('showAlways', False)) and not hide_all_names):
                label = volc_labels[idx]
                plot_item.addItem(label)

        plot_item.getViewBox()

        if flat_data is None:
            # Note: this will likely need to be expanded into a full if statement soon
            # as more than two data types are used, but for now this works.
            sensor = 'tropomi' if height in ['1km', '7km'] else 'omps'
            prestack_file = os.path.join(PRESTACK_PATH, f"{sector['name']}/{sensor}_{period}.stack")

            # Load the pre-stacked data, and bring it up-to-date
            sector_data = _load_prestack_data(prestack_file, lon_from, lon_to,
                                              lat_from, lat_to, all_files, period,
                                              height, sector['name'])

            if not hasattr(sector_data, 'file') or sector_data.file.count() <= 0:
                print(f"No data for sector {sector['name']}, {height}, {period}")
                return

            logging.debug(f"Flattening data for sector {sector['name']}, {height}, {period}")
            flat_data = flatten_data(sector_data)
        else:
            if DATA_DIR == OMPS_DIR:
                OMPS_LOOKUP = {
                    'PBL': 1,
                    'TRL': 3,
                    'TRM': 8,
                    'TRU': 13,
                    'STL': 20,
                }
                so2_col = str(OMPS_LOOKUP[height])
            else:
                so2_col = str(height)

            if not so2_col.endswith('km'):
                so2_col += 'km'

            flat_data['SO2_column_number_density'] = flat_data[f"SO2_number_density_{so2_col}"]

        mass_kt = generate_sector_image(sector_lats, sector_lons, flat_data, file_dir, plot_item,
                                        scale_widget, date_label, period, height,
                                        density=None, filename=img_filename)

        if result_queue is not None:
            sector_id = sector.get('id', sector['name'])
            result_queue.put((sector_id, mass_kt, height))

        print(f"Sector image generation complete for sector {sector['name']}, {height}")

    app.quit()

    del disp_widget
    del app
    del sector_data
    del flat_data


def _load_prestack_data(stack_file, lon_from, lon_to, lat_from, lat_to, all_files,
                        period, height, sector_name):
    # Throughout this function we handle the "No old data" by using try/except
    # blocks. Some might argue that it would be preferable to make a check, and
    # not even try if we don't have data
    _func_start = time.time()

    # Set up the data structure to load from the file
    sector = {
        'data': {}
    }

    sector_data = {}

    # No point in trying to load the prestack file for 1 day, since we only do
    # this every 24 hours - all data loaded would be discarded.
    if period != 'day':
        try:
            with open(stack_file, 'rb') as sector_file:
                # Load the sector parameters
                while True:
                    try:
                        data = pickle.load(sector_file)
                    except EOFError:
                        break

                    if data is None:
                        break
                    key, value = data
                    sector[key] = value

            zarr_file = str(pathlib.Path(stack_file).with_suffix('.zarr'))
            logging.debug("Loading data from prestack file")
            sector_data = xarray.open_zarr(zarr_file).load()
        except (FileNotFoundError, GroupNotFoundError):
            sector_data = {}
            pass  # No data to load. Oh well.

    logging.info(f"Loaded stack data for sector {sector_name} after {time.time()-_func_start}")

    # Figure out how far back we want data for
    start_date = _get_start_dates(period)

    start_idx = None
    # Figure out how much of the existing "stack" we want, based on start of slice.
    try:
        for idx, slice_start in enumerate(map(datetime.fromtimestamp, sector_data['datetime_start'].data)):
            if period == "day":  # day has timezone because we needed to set it to midnight UTC
                slice_start = slice_start.replace(tzinfo=pytz.UTC)
            if slice_start >= start_date:
                # This is the slice where we start caring about the data.
                # Everything before here can be discarded.
                start_idx = idx
                break
    except KeyError:
        pass  # No prestack data loaded. Will need to get from raw files.
    else:
        logging.debug(f"Starting stack for sector {sector_name} from {datetime.fromtimestamp(sector_data['datetime_start'].data[0])} at index {start_idx}")

    logging.debug(f"Found start index for sector {sector_name} after {time.time()-_func_start}")

    # get rid of "old" slices
    # start_idx None means we don't want to use any of the previous loaded data. Oh well.
    current_data = xarray.Dataset()
    if start_idx is not None:
        if start_idx == 0:
            current_data = sector_data  # No change
        else:
            current_data = sector_data.isel(file = slice(start_idx, None))

        # dims = ['file', 'y', 'x', 'corners']
        # for key, value in sector_data.items():
            # if key in ['latitude', 'longitude', 'latitude_bounds', 'longitude_bounds']:
            # if value.shape[0] == 1:
            # value = value.squeeze(axis = 0)

            # current_data.coords[key] = (dims[1:len(value.shape) + 1], value)
            # continue

            # if key in ['datetime_start', 'datetime_length']:
            # current_data.coords[key] = (dims[:len(value.shape)], value[start_idx:])

            # try:
            # current_data[key] = (dims[:len(value.shape)], value[start_idx:])  # Should make a view, not copy the data
            # except IndexError:
            # print(f"Unable to slice value {key}")
            # current_data[key] = value

    del sector_data
    gc.collect()

    try:
        last_data = datetime.fromtimestamp(current_data['datetime_start'][-1].item()) +\
            timedelta(seconds=current_data['datetime_length'][-1].item())
    except KeyError:
        last_data = sector.get('last_data', start_date - timedelta(seconds=1))  # We add a second later, so the
        # default should be 1 second before where we want to begin.

    if DATA_DIR == OMPS_DIR:
        file_suffix = '.h5'
        OMPS_LOOKUP = {
            'PBL': 1,
            'TRL': 3,
            'TRM': 8,
            'TRU': 13,
            'STL': 20,
        }
        so2_col = OMPS_LOOKUP[height]
    else:
        file_suffix = '.nc'
        so2_col = height

    # Could do this as a comprehension, but by reversing the list we arrive
    # at our goal more quickly
    update_files = []
    try:
        comp_date = last_data.date()
    except AttributeError:
        # Assume already a date
        comp_date = last_data
        last_data = datetime.combine(last_data, datetime.max.time())

    for directory in reversed(all_files):
        if parse(directory.split('/')[-1]).date() < comp_date:
            break

        update_files.append(f"{directory}/*{file_suffix}")

    update_files.reverse()

    logging.debug(f"Loading new data for sector {sector_name} from {len(update_files)} files after {time.time()-_func_start}")
    load_start = last_data.timestamp() + 1  # go one second later so we don't get any overlaps
    lats = (lat_from, lat_to)
    longs = (lon_from, lon_to)
    try:
        new_data, coda_file = load_period_data(lats, longs, update_files,
                                               height, start_date=load_start)
    except TypeError:
        logging.debug(f"***No new data for sector {sector_name}, {period}, {height} to add to stack")
        return current_data

    # new_data = [x for x in new_data if x.x.size > 0 and x.y.size > 0]
    if len(new_data) == 0:
        logging.debug(f"***No new data for sector {sector_name}, {period}, {height} to add to stack (new_data is None)")
        return current_data

    logging.debug(f"New data loaded for sector {sector_name}, {period}, {height}, gridding after {time.time()-_func_start}")
    if 'num_lat' not in sector:
        bin_params = _get_bin_parameters(lon_from, lon_to, lat_from, lat_to)
        sector.update(bin_params)

    # Grid the new data, using the same parameters as the existing
    coda_file._bin_spatial(sector['num_lat'], sector['y_from'], sector['y_increment'],
                           sector['num_lon'], sector['x_from'], sector['x_increment'],
                           {'proj': 'merc', 'lon_0': '-90',
                            'preserve_units': False})

    if coda_file._file_data is None:
        # Use -2 rather than -1 because the most recent day may not be complete yet.
        last_data_dir = update_files[-2]
        last_data_date = parse(last_data_dir.split('/')[-2]).date()
        sector['last_data'] = last_data_date
        logging.warning(f"No data found for sector {sector_name}, {period}")
        _save_metadata(sector, stack_file)
        return {}
    else:
        new_data = coda_file._file_data

    # The SO2_column_number_density column is not binned individually, so extract
    # the proper specific altitude column now
    if not str(so2_col).endswith('km'):
        so2_col = str(so2_col) + 'km'

    new_data['SO2_column_number_density'] = new_data[f'SO2_number_density_{so2_col}']

    if DEBUG and 'latitude' not in new_data:
        print(f"*** NO LATITIUDE found in new_data for sector {sector_name}")
        print(f"{new_data}")

    logging.debug(f"New data for sector {sector_name}, {period}, {height} gridded after {time.time()-_func_start}.")

    if not hasattr(current_data, 'file') or current_data.file.count() <= 0:
        current_data = new_data
    else:
        current_data = xarray.concat([current_data, new_data], dim ='file',
                                     coords = 'minimal', compat = "override")
#     for key in new_data:
#         if key not in current_data:
#             current_data[key] = new_data[key]
#         else:
#             current_data[key] = numpy.concatenate((current_data[key], new_data[key]))

    del new_data
    del coda_file

    gc.collect()

    if 'latitude' not in current_data:
        logging.warning(f"***New current_data for sector {sector_name}, {period}, {height} is missing Latitude")
        return {}  # No data to return

    if period != "day":
        # The following is primarily to save memory, disk space, and time when
        # loading. For day stacks that aren't being saved, it's not really worth
        # the effort/time, however negligable.

        # save the updated stack
        logging.debug(f"Saving updated stack for sector {sector_name}, after {time.time()-_func_start}")
        ts1 = time.time()
        _save_metadata(sector, stack_file)
        zarr_file = str(pathlib.Path(stack_file).with_suffix('.zarr'))
        current_data.to_zarr(zarr_file, mode = 'w')

        logging.debug(f"Saved using old method in {time.time()-ts1}")
    logging.info(f"Sector {sector_name} load complete after {time.time()-_func_start}")
    return current_data


def _save_metadata(sector, stack_file):
    with open(stack_file, 'wb') as sector_file:
        for key, value in sector.items():
            if key == 'data':
                continue

            pickle.dump((key, value), sector_file)


def _get_bin_parameters(lon_from, lon_to, lat_from, lat_to):

    # Figure out binning for this sector
    # Bin sizes are in meters
    if DATA_DIR == OMPS_DIR:
        y_increment = 22264
        x_increment = 11132
    else:
        y_increment = 2226.4
        x_increment = 2226.4

    # lat_from is actually the "end" latitude for purposes of drawing the image,
    # so if the window resizes to large, you get a border at the bottom, such that
    # we need to change lat_from to fill the border, not lat_to.
    x_from, y_from = merc_transformer.transform(lat_from - .5, lon_from)
    x_to, y_to = merc_transformer.transform(lat_to, lon_to + .25)
    x_from = math.floor(x_from)

    num_lat = math.ceil((y_to - y_from) / y_increment)
    # Make sure we have enough increments. Should be close, but rounding may make it off a bit.
    while y_from + (y_increment * num_lat) <= y_to:
        print("Not enough lat! Increasing by one!")
        num_lat += 1

    num_lon = math.ceil((x_to - x_from) / x_increment)
    while x_from + (x_increment * num_lon) <= x_to:
        print("Not enough lon! Increasing by one!")
        num_lon += 1

    if DEBUG:
        print(f"Gridding X-From:{x_from} To: {x_from+(x_increment*num_lon)} Target-to: {x_to}")
        print(f"Gridding Y-From:{y_from} To: {y_from+(y_increment*num_lat)} Target-to: {y_to}")

    # Reduce resolution on large sectors
    if num_lat > (IMG_SIZE.height() / 3):
        # Number of latitudes should be no more than about 1/3 the height.
        num_lat = math.ceil(IMG_SIZE.height() / 3)
        y_increment = (y_to - y_from) / (num_lat - 6)
        passes = 0

        # Make sure that the new increment size and number of increments
        # actually gets us to the desired end point
        while (y_from + (y_increment * num_lat)) <= y_to:
            print("Scaled LAT too small! Incrementing", passes, "times.", (y_from + (y_increment * num_lat)), y_to)
            num_lat += 1  # Add one more increment to (hopefully) push us over the threshold
            passes += 1

    if num_lon > (IMG_SIZE.width() / 3):
        num_lon = math.ceil(IMG_SIZE.width() / 3)
        x_increment = (x_to - x_from) / (num_lon - 6)
        while (x_from + (x_increment * num_lon)) <= x_to:
            print("Scaled LON too small!")
            num_lon += 1  # Add one more increment to push us over the threshold

    return {
        'num_lat': num_lat,
        'y_from': y_from,
        'y_increment': y_increment,
        'num_lon': num_lon,
        'x_from': x_from,
        'x_increment': x_increment,
    }


if __name__ == "__main__":
    if DEBUG:
        # from . import wingdbstub
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    start = time.time()

    # Get sectors from DB
    try:
        conn = psycopg2.connect(host='akutan.snap.uaf.edu', database='so2data', user='israel')
    except psycopg2.OperationalError:
        pass  # Default sectors remains as imported from config
    else:
        cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        cur.execute('''SELECT
            id,name,
            latfrom::float "latFrom",
            latto::float "latTo",
            longfrom::float "longFrom",
            longto::float "longTo",
            showalllabels "showAllLabels",
            hidealllabels "hideAllLabels"
        FROM sectors''')
        DEFAULT_SECTORS = cur.fetchall()
        conn.close()

    parser = argparse.ArgumentParser(description="Generate Stacked SO2 images")
    parser.add_argument('-t', metavar='TROPOMI', help="Generate TROPMI images", type=bool,
                        required=False, nargs="?", const=True, default=False)
    parser.add_argument('-o', metavar='OMPS', help="Generate OMPS images", type=bool,
                        required=False, nargs="?", const=True, default=False)
    args = parser.parse_args()

    TEST_SECTORS = [x for x in DEFAULT_SECTORS if x['name'] in ['Cook Inlet']]
    # TEST_SECTORS = DEFAULT_SECTORS

    if args.t:
        print("Generating TROPOMI images")
        if not DEBUG:
            init_image_generation(DEFAULT_SECTORS)
        else:
            init_image_generation(TEST_SECTORS)

    if args.o:
        print("Generating OMPS Images")
        DATA_DIR = OMPS_DIR
        if not DEBUG:
            init_image_generation(DEFAULT_SECTORS, custom_height=['TRL', 'TRM'])
        else:
            init_image_generation(TEST_SECTORS, custom_height=['TRL', 'TRM'])

    print("Completed full run in", time.time() - start)
