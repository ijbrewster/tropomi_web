import collections
import time

from datetime import datetime, timedelta
from cachetools import Cache


def _seconds_till_midnight():
    now = datetime.now()
    midnight = now + timedelta(days = 1)
    midnight = midnight.replace(hour=0, minute=0, second=0, microsecond=0)
    remain = midnight - now
    seconds = remain.seconds
    return seconds


class DateCache(Cache):
    """
    A light override of TTL cache to make the expiration time fall
    on a date boundry instead of after a preset time
    """
    class _Link:

        __slots__ = ("key", "expires", "next", "prev")

        def __init__(self, key=None, expires=None):
            self.key = key
            self.expires = expires

        def __reduce__(self):
            return DateCache._Link, (self.key, self.expires)

        def unlink(self):
            next = self.next
            prev = self.prev
            prev.next = next
            next.prev = prev

    def __init__(self, maxsize, timer=time.monotonic, getsizeof=None):
        # We pass None for the ttl argument of the superclass because we won't be using it.
        super().__init__(maxsize)
        self.timer = timer
        self.__root = root = self._Link()
        root.prev = root.next = root
        self.__links = collections.OrderedDict()
        #self.__data = dict()

    def expire(self, time=None):
        """Remove expired items from the cache."""
        if time is None:
            time = self.timer()
        root = self.__root
        curr = root.next
        links = self.__links
        cache_delitem = Cache.__delitem__
        while curr is not root and not (time < curr.expires):
            cache_delitem(self, curr.key)
            del links[curr.key]
            next = curr.next
            curr.unlink()
            curr = next

    def __setitem__(self, key, value, cache_setitem=Cache.__setitem__):
        self.expire(self.timer())
        time = self.timer()
        cache_setitem(self, key, value)
        try:
            link = self.__getlink(key)
        except KeyError:
            self.__links[key] = link = self._Link(key)
        else:
            link.unlink()
        link.expires = time + _seconds_till_midnight()
        link.next = root = self.__root
        link.prev = prev = root.prev
        prev.next = root.prev = link

    def __getlink(self, key):
        value = self.__links[key]
        self.__links.move_to_end(key)
        return value

    def __delitem__(self, key, cache_delitem=Cache.__delitem__):
        cache_delitem(self, key)
        link = self.__links.pop(key)
        link.unlink()
        if not (self.timer() < link.expires):
            raise KeyError(key)

    def __getitem__(self, key, cache_getitem=Cache.__getitem__):
        try:
            link = self.__getlink(key)
        except KeyError:
            expired = False
        else:
            expired = not (self.timer() < link.expires)

        if expired:
            self.__delitem__(key)

        return cache_getitem(self, key)


