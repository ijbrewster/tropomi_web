from . import app

import flask
import pyproj
import requests

LAT_LON_PROJ = pyproj.Proj('epsg:4326', preserve_units=False)

# ####### Sample sector definition
# {'sectorGroup': '5 km sectors',
 # 'sectorLabel': 'Western North Pacific',
 # 'sector': '5kmRUNP',
 # 'centerLat': '55.00000',
 # 'centerLng': '170.00000',
 # 'pixelSize': '5.00',
 # 'imageHeight': '800',
 # 'imageWidth': '1000',
 # 'sectorId': '31',
 # 'caseStudyInd': 'N'}
#########################
 

@app.route('/vv_sectors')
def get_sectors():
    response = requests.get('https://volcview.wr.usgs.gov/vv-api/sectorApi/all')
    if response.status_code != 200:
        return flask.abort(response.status_code)
    
    sectors = response.json()
    sectors_with_corners = [gen_corners(sector) for sector in sectors]
    return flask.jsonify(sectors_with_corners)

def gen_corners(sector):
    # Create a transformer object to transform our coordinates to/from latitude/longitude
    _proj_str = f'+proj=laea +lat_0={sector["centerLat"]} +lon_0={sector["centerLng"]} +x_0=0 +y_0=0 +ellps=WGS84 +units=m +no_defs'
    _laea_transformer = pyproj.Transformer.from_proj(_proj_str, LAT_LON_PROJ)
    
    meter_width = float(sector['pixelSize']) * 1000 * int(sector['imageWidth'])  # km-->meters
    meter_height = float(sector['pixelSize']) * 1000 * int(sector['imageHeight'])    
    
    # Our lat/lon0 for the projection is the sector center, so our laea range is just the 
    # specified number of meters around 0
    x_range = [0 - (meter_width / 2), (meter_width / 2)]
    y_range = [0 - (meter_height / 2), (meter_height / 2)]

    laea_corners = [
        # UL, UR, LL, LR
        [x_range[0], y_range[1]],
        [x_range[1], y_range[1]],
        [x_range[0], y_range[0]],
        [x_range[1], y_range[0]]
    ]
    
    laea_x,laea_y = zip(*laea_corners)
    
    lat_corners,lon_corners = _laea_transformer.transform(laea_x, laea_y)
    latlon_corners = tuple(zip(lat_corners, lon_corners))
    
    corners = {
        'UL': latlon_corners[0],
        'UR': latlon_corners[1],
        'LL': latlon_corners[2],
        'LR': latlon_corners[3],
    }
    
    sector['sectorCorners'] = corners
    
    return sector
    
